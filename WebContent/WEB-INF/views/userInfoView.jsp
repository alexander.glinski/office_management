<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>User Info</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
     <c:if test="${isEmailSend != null}">
    <p style="color: red;">Email ${isEmailSend}</p>
    </c:if>
     
    <h3>Hello: ${user.userName}</h3>
 
    User Name: <b>${user.userName}</b>
    <br />
    Gender: ${user.gender } <br />
    Email: ${user.email } <br />
    <c:if test="${user.isEmailConfirmed eq \"NO\"}">
    <form action="sendEmail">
    <input type="hidden" name="userName" value="${user.userName}">
    <input type="submit" id="submit" style="display: none;">
    </form>
    Вам не доступен некоторый функционал. <br>
    Подтвертите почту<input type="button" id="sendEmail" value="Подтвердить">   
    </c:if>
    <h3>Your orders</h3>
    <table border="1" >
       <tr>
          <th>Order Number</th>
          <th>Product Code</th>
          <th>Order Date</th>
          <th>Required Date</th>                   
          <th>Shipped Date</th>
          <th>Status</th>          
          <th>Cancel the order</th>
       </tr>
       <c:forEach items="${orderList}" var="order" >
          <tr>
             <td>${order.orderNumber}</td>
             <td>${order.productCode}</td>
             <td>${order.orderDate}</td>
             <td>${order.requiredDate}</td>
             <td>${order.shippedDate}</td>
             <td>${order.status}</td>
             <c:if test="${order.status ne \"Shipped\" && order.status ne \"Отменён\"}"> 
             <td><a href="cancelOrder?orderNumber=${order.orderNumber}"><input type="button" value="Cancel order"></a></td>
             </c:if>
             <c:if test="${order.status eq \"Отменён\"}"> 
             <td>The order has been cancelled</td>
             </c:if>
             <c:if test="${order.status eq \"Shipped\"}"> 
             <td><a href="reportProblem?orderNumber=${order.orderNumber}"><input type="button" value="Report a problem"></a></td>
             </c:if>           
          </tr>
       </c:forEach>
    </table>
    
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
 <script type="text/javascript">

    document.getElementById("sendEmail").addEventListener("click", function(){
    		 document.getElementById('submit').click();
	  })
	  
</script>
</html>