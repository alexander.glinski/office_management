<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>Bar Chart</title>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  </head>
  
  <style>
  .rating-histogram {
  padding: 5px 5px 5px 44px;
  text-align: left;
  width: 100%;
}

.rating-histogram .rating-bar-container {
  position: relative;
  margin-bottom: 5px;
}

.rating-histogram .bar-label {
  margin-right: 10px;
  position: absolute;
  left: -44px;
  top: 2px;
}

.rating-histogram .bar-label:before {
  content: "\f005";
  font-family: FontAwesome;
  font-size: 16px;
  line-height: 16px;
  height: 16px;
  width: 16px;
  color: #ccc;
  display: inline-block;
  margin-right: 5px;
}

.rating-histogram .bar {
  background-color: #8e70af;
  background-image: -webkit-linear-gradient(left, #8e70af, #48bfed);
  background-image: linear-gradient(to right, #8e70af, #48bfed);
  -webkit-transition: width 2s ease;
  -moz-transition: width 2s ease;
  transition: width 2s ease;
  opacity: .8;
  display: inline-block;
  vertical-align: middle;
  width: 1%;
  height: 15px;
  margin-right: 3px;
}

.rating-histogram .bar-number {
  font-size: 14px;
  line-height: 1;
  vertical-align: middle;
}
  </style>
  <body>
     <jsp:include page="_header.jsp"></jsp:include>
     <jsp:include page="_menu.jsp"></jsp:include>
     <c:if test="${errorString ne null}">
    <p style="color: red;"><%=request.getAttribute("errorString")%></p>
    </c:if>
     <h3>Введите целые положительные числа через пробелл</h3>
     <form name="MyForm" action="barChart" method="POST">
          <input type="text" name="valuesOfBarChart"><input type="submit"> 
     </form>
     <c:if test="${values ne null}">
      <div class="rating-histogram">
     <c:forEach items="${values}" var="val" varStatus="i">
             <div class="rating-bar-container ${i.count}" data-id="${i.count}">
    <span class="bar-label"> ${i.count}</span>
    <span class="bar"></span>
    <span class="bar-number"></span>
  </div>
  <input type="hidden" class="reviews_${i.count}star" value="${val}">
     </c:forEach>
      
</div><!-- /rating-histogram -->
</c:if>
     <jsp:include page="_footer.jsp"></jsp:include>
 
  </body>
  
   <script type="text/javascript">
  $(function() {
	  var stars = new Array();
	  var sum = 0;
	  var width = new Array();
      var count = ${valuesLength};
	  for ( var i = 1; i <= count; i++ ) {
	    stars.push(parseInt($('.reviews_'+i+'star').val()));
	  }     

	  for ( var i = 0; i < stars.length; i++ ) {
	    sum += stars[i];       
	  }     

	  for ( var i = 0; i < stars.length; i++ ) {
	    w = ((stars[i]) / sum * 100).toFixed(0);
	    width.push(w);
	    $('.rating-bar-container[data-id='+(i+1)+'] .bar').css('width', w+'%' ); 
	  }

	  if (sum > 0) {
	    for ( var i = 0; i < stars.length; i++ ) {
	      $('.rating-bar-container[data-id='+(i+1)+'] .bar-number').html(width[i]+'%'); 
	    }
	  } else{
	    $(".rating-bar-container .bar-number").html('0%')
	  }
	});
  </script> 
</html>