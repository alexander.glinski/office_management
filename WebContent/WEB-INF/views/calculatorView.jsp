<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Calculator test</title>
</head>
<body>

      <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
      <h1>Добрый день! Проверьте свои знания арифметики</h1>
      <div>
      Количество правильных ответов <%=request.getAttribute("amountOfTrue")%>  из <%=request.getAttribute("amount")%> ( <%=request.getAttribute("thePercentageOfCorrect")%> %)
      </div> 
       <form name="myForm" action="" method="POST" onsubmit="calculator">
       <input type="text" name="amountOfTrue" style="display: none;" value="<%=request.getAttribute("amountOfTrue")%>">
       <input type="text" name="amount" style="display: none;" value="<%=request.getAttribute("amount")%>">
       <input type="text" name="thePercentageOfCorrect" style="display: none;" value="<%=request.getAttribute("thePercentageOfCorrect")%>">
       <input type="text" name="result" style="display: none;" value="<%=request.getAttribute("result")%>">
       
            <table border="0">          
                <tr>
                     <td>
                     Сейчас проверяется:
                     </td>
                     <td>
                     <input type="radio" id="addition" name="function" value="addition" <%=request.getAttribute("checkedAddition")%> disabled="disabled"/>
                    <label for="addition">Сложение</label>
                    <input type="radio" id="subtraction" name="function" value="subtraction" <%=request.getAttribute("checkedSubtraction")%> disabled="disabled"/>
                    <label for="subtraction">Вычитание</label>
                    <input type="radio" id="multiplication" name="function" value="multiplication" <%=request.getAttribute("checkedMultiplication")%> disabled="disabled"/>
                    <label for="multiplication">Умножение</label>
                    <input type="radio" id="division" name="function" value="division" <%=request.getAttribute("checkedDivision")%> disabled="disabled"/>
                    <label for="division" >Деление</label>
                      </td>              
                </tr>
                <tr>
                     <td colspan="2">
                     Сколько будет <%=request.getAttribute("task") %>?
                     </td>
                </tr>
                <tr>
                     <td >
                     
                     <input type="radio" id="first" name="answer"  value="<%=request.getAttribute("first") %>" onclick="document.getElementById('submit').click();"/>
                     <label for="first" ><%=request.getAttribute("first") %></label> <br>                   
                     <input type="radio" id="second" name="answer" value="<%=request.getAttribute("second") %>" onclick="document.getElementById('submit').click();"/>
                     <label for="second" ><%=request.getAttribute("second") %></label><br>  
                     <input type="radio" id="third" name="answer" value="<%=request.getAttribute("third") %>" onclick="document.getElementById('submit').click();"/>
                     <label for="third" ><%=request.getAttribute("third") %></label><br>  
                     <input type="radio" id="fourth" name="answer" value="<%=request.getAttribute("fourth") %>" onclick="document.getElementById('submit').click();"/>
                     <label for="fourth" ><%=request.getAttribute("fourth") %></label> <br>                   
                     </td>
                     <td>
                     <%=request.getAttribute("message") %>
                     </td>
                </tr>
                <tr>
                    <td>
                    <input type="button" id="nextTaskButton" name="nextTask" value="Следующее задание" onclick="document.getElementById('submit').click();">
                    <input type="hidden" id="nextTask" name="isNextTask">
                    </td>
                    <td>
                    <input type="button" id="endTasks" value="Закончить тестирование">
                    <input type="hidden" id="isEnd" name="isEndTasks">
                    </td>
                </tr>
            </table>
               
               <input type="submit" id="submit" style="display: none;">
       </form> 
             <jsp:include page="_footer.jsp"></jsp:include>  
</body>
<script type="text/javascript">

    document.getElementById("endTasks").addEventListener("click", function(){
    	if(confirm("Вы точно хотите закончить тестирование?")) {
    	   	 document.getElementById('isEnd').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('isEnd').value = "false";
    	}

	  })
	  
	  document.getElementById("nextTaskButton").addEventListener("click", function(){
    	if(confirm("Вы точно хотите перейти к следующему заданию?")) {
    	   	 document.getElementById('nextTask').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('nextTask').value = "false";
    	}

	  })
</script>
</html>