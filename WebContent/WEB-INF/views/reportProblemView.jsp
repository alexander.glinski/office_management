<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report problem</title>
</head>
<body>
             <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
     <form name="myForm" action="reportProblem" method="POST">
        <table>
           <tr>
              <th>Who is Send</th>
              <th>Your problem</th>
              <th>Send</th>
           </tr>
           
           <tr>
              <td><%=request.getAttribute("userName") %></td>
              <td><textarea name="problem" cols="140" rows="5"></textarea></td>
              <td><input type="submit" value="Submit"></td>
           </tr>
        </table>
        <input type="hidden" name="orderNumber" value="<%=request.getAttribute("orderNumber") %>">
        <input type="hidden" name="userName" value="<%=request.getAttribute("userName") %>">
     </form>
        <jsp:include page="_footer.jsp"></jsp:include> 
</body>
</html>