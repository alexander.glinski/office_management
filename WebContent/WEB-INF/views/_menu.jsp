<%@page import="com.itstep.retailer.beans.UserAccount"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

    
<div class="navigation" style="padding: 5px;">
 
   <a href="${pageContext.request.contextPath}/">Home</a>
   |
   <a href="${pageContext.request.contextPath}/productList">Product List</a>
   |
   <a href="${pageContext.request.contextPath}/userInfo">My Account Info</a>
   |
   <a href="${pageContext.request.contextPath}/login">Login</a>   
   
   <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10 }">
          |
          <a href="${pageContext.request.contextPath}/productLinesList">Product Lines List</a>
          |
          <a href="${pageContext.request.contextPath}/employeeList">Employee List</a>
          |
          <a href="${pageContext.request.contextPath}/shop">Shop</a>
          |
          <a href="${pageContext.request.contextPath}/myCourses">My Courses</a>
          |
          <a href="${pageContext.request.contextPath}/applyStudents?course=${loginedUser.role.id}">Apply Students</a>   
   </c:if>
   <c:if test="${loginedUser.role.id eq 2}">
          |
          <a href="${pageContext.request.contextPath}/orders">Orders</a>
   </c:if>
   <c:if test="${loginedUser.role.id eq 3}">
          |
          <a href="${pageContext.request.contextPath}/showCourses">Show Courses</a>
   </c:if>
      |
   <a href="${pageContext.request.contextPath}/calculator">Test</a> 
      

    
</div>  