<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>No functional</title>
  </head>
  <body>
     <jsp:include page="_header.jsp"></jsp:include>
     <jsp:include page="_menu.jsp"></jsp:include>
     <h3>Вам не доступен данный функционал</h3>
     <a href="userInfo">Cancel</a>
     <jsp:include page="_footer.jsp"></jsp:include>
 
  </body>
</html>