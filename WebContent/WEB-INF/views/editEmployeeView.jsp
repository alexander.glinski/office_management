<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <title>Edit Product</title>
   </head>
   <body>
 
      <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
 
      <h3>Edit Employee</h3>
 
      <p style="color: red;">${errorString}</p>     
 
      <c:if test="${not empty employee}">
         <form method="POST" action="${pageContext.request.contextPath}/editEmployee">
            <input type="hidden" name="code" value="${employee.employeeNumber}" />
            <table border="0">
               <tr>
                  <td>EmployeeNumber</td>
                  <td style="color:red;">
                  ${employee.employeeNumber}
                  <input type="hidden" name="employeeNumber" value="${employee.employeeNumber}" />
                  </td>
               </tr>
               <tr>
                  <td>lastName</td>
                  <td><input type="text" name="lastName" value="${employee.lastName}" /></td>
               </tr>
               <tr>
                  <td>firstName</td>
                  <td><input type="text" name="firstName" value="${employee.firstName}" /></td>
               </tr>

               <tr>
                  <td>extension</td>
                  <td><input type="text" name="extension" value="${employee.extension}" /></td>
               </tr>
               <tr>
                  <td>email</td>
                  <td><input type="text" name="email" value="${employee.email}" /></td>
               </tr>
               <tr>
                  <td>officeCode</td>
                  <td style="color:red;">
                  ${employee.officeCode}
                  <input type="hidden" name="officeCode" value="${employee.officeCode}" />
                  </td>                 
               </tr>
                              <tr>
                  <td>reportsTo</td>
                  <td><input type="text" name="reportsTo" value="${employee.reportsTo}" /></td>
               </tr>
                              <tr>
                  <td>jobTitle</td>
                  <td><input type="text" name="jobTitle" value="${employee.jobTitle}" /></td>
               </tr>
                              <tr>
                  <td>lastUpdated</td>
                  <td><input type="text" name="lastUpdated" value="${employee.lastUpdated}" /></td>
               </tr>
               <tr>
                  <td>Reason?</td>
                  <td><input type="text" name="reason" /></td>
               </tr>
               <tr>
                  <td colspan = "2">
                      <input type="submit" value="Submit" />
                      <a href="${pageContext.request.contextPath}/employeeList">Cancel</a>
                  </td>
               </tr>
            </table>
            
         </form>
      </c:if>
 
      <jsp:include page="_footer.jsp"></jsp:include>
 
   </body>
</html>