<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>User Info</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
    <h3>Orders</h3>
    <table border="1" >
       <tr>
          <th>Order Number</th>
          <th>Product Code</th>
          <th>Order Date</th>
          <th>Required Date</th>                   
          <th>Shipped Date</th>
          <th>Status</th>
          <th>Take/Ship Order</th>
       </tr>
       <c:forEach items="${orderList}" var="order" >
          <tr>
             <td>${order.orderNumber}</td>
             <td>${order.productCode}</td>
             <td>${order.orderDate}</td>
             <td>${order.requiredDate}</td>
             <td>${order.shippedDate}</td>
             <td>${order.status}</td>  
             <c:if test="${order.status eq \"Ждет принятия\"}">
             <td>
             <a href="takeOrder?orderNumber=${order.orderNumber}"><input type="button" value="Take Order"></a>
             </td>  
             </c:if>
             <c:if test="${order.status eq \"В обработке\"}">
             <td>
             <a href="shipOrder?orderNumber=${order.orderNumber}"><input type="button" value="Ship Order"></a>
             </td>  
             </c:if>       
          </tr>
       </c:forEach>
    </table>
    
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
 <script type="text/javascript">

    document.getElementById("sendEmail").addEventListener("click", function(){
    		 document.getElementById('submit').click();
	  })
	  
</script>
</html>