<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title> Student</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>Students</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>name</th>
          <th>surName</th>
          <th>age</th>
          <th>email</th>
          <c:if test="${loginedUser.role.id eq 1 }">
                    <th>Apply Student</th>                   
          </c:if>
          <c:if test="${loginedUser.role.id eq 1 }">
                    <th>Отклонить</th>                   
          </c:if>
       </tr>
       <c:forEach items="${studentsList}" var="student" >
          <tr>
             <td>${student.name}</td>
             <td>${student.surName}</td>
             <td>${student.age}</td>
             <td>${student.email}</td>             
            <c:if test="${loginedUser.role.id eq 1 }">
             <td><a href="applStudent?id=${student.id}&course=${student.course}&age=${student.age}&email=${student.email}&name=${student.name}&surName=${student.surName}"><input type="button" value="Apply"></a></td>                   
           </c:if>
           <c:if test="${loginedUser.role.id eq 1 }">
             <td><a href="dltStudent?id=${student.id}&course=${student.course}"><input type="button" value="Отклонить"></a></td>                   
           </c:if>
          </tr>
       </c:forEach>
    </table>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>