<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Employee List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>Office</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>officeCode</th>
          <th>city</th>
          <th>phone</th>
          <th>addressLine1</th>
          <th>addressLine2</th>
          <th>state</th>
          <th>country</th>
          <th>postalCode</th>
          <th>territory</th>
       </tr>
          <tr>
             <td>${office.officeCode}</td>
             <td>${office.city}</td>
             <td>${office.phone}</td>
             <td>${office.addressLine1}</td>
             <td>${office.addressLine2}</td>
             <td>${office.state}</td>
             <td>${office.country}</td>
             <td>${office.postalCode}</td>
             <td>${office.territory}</td>
          </tr>
    </table>
 
     <a href="employeeList">Back</a>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>