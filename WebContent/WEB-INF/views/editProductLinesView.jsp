<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <title>Edit Product Lines</title>
   </head>
   <body>
 
      <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
 
      <h3>Edit Product</h3>
 
      <p style="color: red;">${errorString}</p>
 
      <c:if test="${not empty productLines}">
         <form method="POST" action="${pageContext.request.contextPath}/editProductLines">
            <input type="hidden" name="productLine" value="${productLines.productLine}" />
            <table border="0">
               <tr>
                  <td>Code</td>
                  <td style="color:red;">${productLines.productLine}</td>
               </tr>
               <tr>
                  <td>textDescription</td>
                  <td><input type="text" name="textDescription" value="${productLines.textDescription}" /></td>
               </tr>
               <tr>
                  <td>htmlDescription</td>
                  <td><input type="text" name="htmlDescription" value="${productLines.htmlDescription}" /></td>
               </tr>
               <tr>
                  <td>image</td>
                  <td><input type="text" name="image" value="${productLines.image}" /></td>
               </tr>
               <tr>
                  <td colspan = "2">
                      <input type="submit" value="Submit" />
                      <a href="${pageContext.request.contextPath}/productLinesList">Cancel</a>
                  </td>
               </tr>
            </table>
         </form>
      </c:if>
 
      <jsp:include page="_footer.jsp"></jsp:include>
 
   </body>
</html>