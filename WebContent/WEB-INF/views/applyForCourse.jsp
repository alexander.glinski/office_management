<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        
    <title>Register</title>
    
    <style type="text/css">
        <%@include file="css/main.css" %>
    </style>
    
    <style type="text/css">
        <%@include file="css/login-register.css" %>
    </style>
   
</head>

<body>
 <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
    <div class="main-content">
        <form class="form-register" method="post" action="applyForCourses?course=${course}" >
            <div class="form-register-with-email">
                <div class="form-white-background">
                    <div class="form-title-row">
                    </div>
                    <p style="color:green">                     
                    <%
                    if(request.getAttribute("successMsg")!=null)
                    {
                        out.println(request.getAttribute("successMsg")); //register success message
                    }
                    %>
                    </p>
 
                   <br>
                   
                    <div class="form-row">
                        <label>
                            <span>Course</span>
                            <input type="text" name="course" id="userName" value="<%=request.getAttribute("course") %>" disabled="disabled">
                        </label>
                    </div>
                    <div class="form-row">
                        <label>
                            <span>Name</span>
                            <input type="text" name="name" id="gender" placeholder="enter name">
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>SurName</span>
                            <input type="text" name="surName" id="email" placeholder="enter surName">
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Age</span>
                            <input type="text" name="age" id="password" placeholder="enter age">
                        </label>
                    </div>
                    
                     <div class="form-row">
                        <label>
                            <span>Email</span>
                            <input type="text" name="email" id="password2" placeholder="enter email">
                        </label>
                    </div>

                    <input type="submit" name="btn_register" value="Submit">
                    
                </div>
            </div>

        </form>

    </div>
<jsp:include page="_footer.jsp"></jsp:include>
</body>

</html>