<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title> Student</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>Students</h3>
 
    <p style="color: red;">${errorString}</p>
    <form action="closeCourse?id=${loginedUser.role.id}" method="post">
    

    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>name</th>
          <th>surName</th>
          <th>age</th>
          <th>email</th>
          <th>mark</th>
       </tr>
       <c:forEach items="${studentsList}" var="student" varStatus="s">
          <tr>
             <td>${student.name}</td>
             <td>${student.surName}</td>
             <td>${student.age}</td>
             <td>${student.email}</td>             
            <td><input type="text" name="mark${s.count}"></td>
            

          </tr>
       </c:forEach>
    </table>
    <input type="submit" value="Close course">
     </form>
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>