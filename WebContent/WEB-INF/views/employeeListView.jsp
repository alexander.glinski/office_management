<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Employee List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>Employee List</h3>
 
    <p style="color: red;">${errorString}</p>
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>employeeNumber</th>
          <th>lastName</th>
          <th>firstName</th>
          <th>extension</th>
          <th>email</th>
          <th>office</th>
          <th>reportsTo</th>
          <th>jobTitle</th>
          <th>lastUpdated</th>
          <th>Edit</th>
          <th>Delete</th>
       </tr>
       <c:forEach items="${employeeList}" var="employee" >
          <tr>
             <td>${employee.employeeNumber}</td>
             <td>${employee.lastName}</td>
             <td>${employee.firstName}</td>
             <td>${employee.extension}</td>
             <td>${employee.email}</td>
             <td><a href="viewOffice?employeeNumber=${employee.employeeNumber}">View Office</a></td>
             <td>${employee.reportsTo}</td>
             <td>${employee.jobTitle}</td>
             <td>${employee.LAST_UPDATED}</td>
             <td>
                <a href="editEmployee?employeeNumber=${employee.employeeNumber}">Edit</a>
             </td>
             <td>
                <a href="SureDeleteEmployee?employeeNumber=${employee.employeeNumber}">Delete</a>
             </td>
          </tr>
       </c:forEach>
    </table>
 <button></button>
    <a href="createEmployee" >Create Employee</a>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>