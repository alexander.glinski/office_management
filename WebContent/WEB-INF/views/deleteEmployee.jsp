<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>Home Page</title>
  </head>
  <body>
     <jsp:include page="_header.jsp"></jsp:include>
     <jsp:include page="_menu.jsp"></jsp:include>
     
     <form name="myForm" action="deleteEmployee?employeeNumber=${employee.employeeNumber}">
          <h3>Are you sure?</h3>
          <input type="hidden" name="employeeNumber" value="${employee.employeeNumber}">
     Reason?<input type="text" name="reason" /><br>         
     <input type="button" id="delete" value="Delete?">  <a href="employeeList">Cancel</a> 
     <input type="submit" id="submit" style="display: none;">
     </form>
      <jsp:include page="_footer.jsp"></jsp:include>
  </body>
  
  <script type="text/javascript">

    document.getElementById("delete").addEventListener("click", function(){
    		 document.getElementById('submit').click();

	  })
	  
</script>
</html>