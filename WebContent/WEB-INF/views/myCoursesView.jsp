<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title> My Courses</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>My Courses</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>Name of Courses</th>
          <th>Date of Start</th>
          <th>Date of End</th>
          <th>Посмотреть студентов</th>
          <th>Набор</th>
       </tr>
       <c:forEach items="${coursesList}" var="course" >
          <tr>
             <td>${course.nameOfCourse}</td>
             <td>${course.dateOfStart}</td>
             <td>${course.dateOfStart}</td>
             <td>
                <a href="showStudents?course=${course.id}"><input type="button" value="Посмотреть студентов"></a>
             </td>
            <c:if test="${course.status eq \"Идет набор\" }">
            <td>
                <a href="closeCourse?id=${course.id}"><input type="button" value="Закрыть курс"></a>
             </td>
            </c:if>
            <c:if test="${course.status eq \"Набор Закончен\" }">
            <td>
                <a href="startCourse?id=${course.id}"><input type="button" value="Начать набор"></a>
             </td>
            </c:if>
          </tr>
       </c:forEach>
    </table>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>