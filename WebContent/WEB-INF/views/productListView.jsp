<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Product List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>Product List</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>Code</th>
          <th>Name</th>
          <th>Price</th>
          <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10 ||loginedUser.role.id eq 2 }">
                    <th>Edit</th>                   
          </c:if>
          <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10}">
                    <th>Delete</th>
          </c:if>
          <c:if test="${loginedUser.role.id eq 3}">
                    <th>Buy</th>
          </c:if>
       </tr>
       <c:forEach items="${productList}" var="product" >
          <tr>
             <td>${product.code}</td>
             <td>${product.name}</td>
             <td>${product.price}</td>
             <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10 ||loginedUser.role.id eq 2 }">
                          <td>
                <a href="editProduct?code=${product.code}">Edit</a>
             </td>
             </c:if>
              <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10}">
                          <td>
                <a href="sureDeleteProduct?code=${product.code}">Delete</a>
             </td>
             </c:if>
             <c:if test="${loginedUser.role.id eq 3}">
                <td>
                 <a href="buyProduct?code=${product.code}&userName=${loginedUser.userName}"><input type="button" value="Buy"></a>
                </td>
             </c:if>
          </tr>
       </c:forEach>
    </table>
     <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10}">
         <a href="createProduct" >Create Product</a>
    </c:if>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>