<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Result test</title>
</head>
<body>
       <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
       <form onsubmit="calculator">
       <h3>Поздравляем, вы прошли тест!</h3>
       <h3>Ваш результат <%=request.getAttribute("thePercentageOfCorrect")%>%</h3>
       <h3>Оценка - <%=request.getAttribute("mark") %></h3>      
       <input type="submit" value="Пройти тест еще раз">
       </form>
       <jsp:include page="_footer.jsp"></jsp:include>  
</body>
</html>