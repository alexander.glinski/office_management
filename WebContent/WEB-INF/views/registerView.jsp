<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        
    <title>Register</title>
    
    <style type="text/css">
        <%@include file="css/main.css" %>
    </style>
    
    <style type="text/css">
        <%@include file="css/login-register.css" %>
    </style>

    
    <!-- javascript for registeration form validation-->
    <script>    
    
        function validate()
        {
            var userName_p= /^[a-z A-Z]+$/; //pattern allowed alphabet a-z or A-Z 
            var gender_p= /^[a-z A-Z]+$/; //pattern allowed alphabet a-z or A-Z 
            var email_valid= /^[\w\d\.]+\@[a-zA-Z\.]+\.[A-Za-z]{1,4}$/; //pattern valid email validation
            var password_valid=/^[A-Z a-z 0-9 !@#$%&*()<>]{6,12}$/; //pattern password allowed A to Z, a to z, 0-9, !@#$%&*()<> charecter 
            
            var userName = document.getElementById("userName"); //textbox id fname
            var gender = document.getElementById("gender"); //textbox id lname
            var email = document.getElementById("email"); //textbox id email
            var password = document.getElementById("password"); //textbox id password
            
            if(!userName_p.test(userName.value) || userName.value=='') 
            {
                alert("Enter Firstname Alphabet Only....!");
                userName.focus();
                userName.style.background = '#f08080';
                return false;                    
            }
            if(!gender_p.test(gender.value) || gender.value=='') 
            {
                alert("Enter Lastname Alphabet Only....!");
                gender.focus();
                gender.style.background = '#f08080';
                return false;                    
            }
            if(!email_valid.test(email.value) || email.value=='') 
            {
                alert("Enter Valid Email....!");
                email.focus();
                email.style.background = '#f08080';
                return false;                    
            }
            if(!password_valid.test(password.value) || password.value=='') 
            {
                alert("Password Must Be 6 to 12 and allowed !@#$%&*()<> character");
                password.focus();
                password.style.background = '#f08080';
                return false;                    
            }
        }
        
    </script>   

</head>

<body>
 <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
    <div class="main-content">
        <form class="form-register" method="post" action="reg" onsubmit="return validate();">
            <div class="form-register-with-email">
                <div class="form-white-background">
                    <div class="form-title-row">
                          <p style="color: red;">${errorString}</p>
                        <h1>Register</h1>
                    </div>
                    <p style="color:green">                     
                    <%
                    if(request.getAttribute("successMsg")!=null)
                    {
                        out.println(request.getAttribute("successMsg")); //register success message
                    }
                    %>
                    </p>
 
                   <br>
                   
                    <div class="form-row">
                        <label>
                            <span>USER NAME</span>
                            <input type="text" name="txt_userName" id="userName" placeholder="enter user name">
                        </label>
                    </div>
                    <div class="form-row">
                        <label>
                            <span>GENDER</span>
                            <input type="text" name="txt_gender" id="gender" placeholder="enter gender">
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Email</span>
                            <input type="text" name="txt_email" id="email" placeholder="enter email">
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Password</span>
                            <input type="password" name="txt_password" id="password" placeholder="enter password">
                        </label>
                    </div>
                    
                     <div class="form-row">
                        <label>
                            <span>Password</span>
                            <input type="password" name="txt_password2" id="password2" placeholder="re-enter password">
                        </label>
                    </div>

                    <input type="submit" name="btn_register" value="Register">
                    
                </div>
                
                <a href="/app/login" class="form-log-in-with-existing">Already have an account? <b> Login here </b></a>

            </div>

        </form>

    </div>
<jsp:include page="_footer.jsp"></jsp:include>
</body>

</html>
