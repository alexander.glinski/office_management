<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>Welcome</title>
  </head>
  <body>
     <jsp:include page="_header.jsp"></jsp:include>
     <jsp:include page="_menu.jsp"></jsp:include>
     <h3>Ваша заявка отправлена. Ждите ответа</h3>
     <a href="showCourses">Cancel</a>
     <jsp:include page="_footer.jsp"></jsp:include>
 
  </body>
</html>