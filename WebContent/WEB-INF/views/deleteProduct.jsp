<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
     <meta charset="UTF-8">
     <title>Home Page</title>
  </head>
  <body>
     <jsp:include page="_header.jsp"></jsp:include>
     <jsp:include page="_menu.jsp"></jsp:include>
     <h3>Are you sure?</h3>
     <a href="deleteProduct?code=${product.code}">Delete</a>  <a href="productList">Cancel</a> 
     <jsp:include page="_footer.jsp"></jsp:include> 
  </body>
</html>