<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shop</title>
</head>
<body>
      <jsp:include page="_header.jsp"></jsp:include>
      <jsp:include page="_menu.jsp"></jsp:include>
      <form name="myForm" action="saveCookieServlet" method="POST">
           <table>
                 <tr>
                 <td colspan="2">
                 Продукты
                 </td>
                 </tr>
                 <tr>
                 <td >
                 Мясо
                 </td>
                 <td >
                 <input type="button" id="buyMeat" value="Купить">
                 <input type="hidden" id="isBuyMeat" name="isBuyMeat">
                 </td>
                 </tr>
                 <tr>
                 <td >
                 Рыба
                 </td>
                 <td >
                 <input type="button" id="buyFish" value="Купить">
                 <input type="hidden" id="isBuyFish" name="isBuyFish">
                 </td>
                 </tr>
                 <tr>
                 <td >
                 Пиво
                 </td>
                 <td >
                 <input type="button" id="buyBeer" value="Купить">
                 <input type="hidden" id="isBuyBeer" name="isBuyBeer">
                 </td>
                 </tr>
           </table>    
         <input type="submit" id="submit" style="display: none;">      
      </form>
      <div <%=request.getAttribute("discountForMeat") %>>
           <h3>Вы купили что-то из продуктов, не хотите ли еще купить мясо, но уже с 10% скидкой?</h3>
           <input type="button" id="buyMeat2" value="Купить">
      </div>
      
       <div <%=request.getAttribute("discountForFish") %>>
          <h3>Вы купили что-то из продуктов, не хотите ли еще купить рыбу, но уже с 10% скидкой?</h3>
                            <input type="button" id="buyFish2" value="Купить">
      </div>

       <div <%=request.getAttribute("discountForBeer") %>>
           <h3>Вы купили что-то из продуктов, не хотите ли еще купить пиво, но уже с 10% скидкой?</h3>
           <input type="button" id="buyBeer2" value="Купить">
      </div>
       <jsp:include page="_footer.jsp"></jsp:include>  
</body>

<script type="text/javascript">

    document.getElementById("buyMeat").addEventListener("click", function(){
    	if(confirm("Точно купить мяско?")) {
    	   	 document.getElementById('isBuyMeat').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('isBuyMeat').value = "false";
    	}

	  })
	  document.getElementById("buyMeat2").addEventListener("click", function(){
    	if(confirm("Точно купить мяско?")) {
    	   	 document.getElementById('isBuyMeat').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('isBuyMeat').value = "false";
    	}

	  })
	  
	      document.getElementById("buyFish").addEventListener("click", function(){
    	if(confirm("Точно купить рыбу?")) {
    	   	 document.getElementById('isBuyFish').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('isBuyFish').value = "false";
    	}

	  })
	  document.getElementById("buyFish2").addEventListener("click", function(){
    	if(confirm("Точно купить рыбу?")) {
    	   	 document.getElementById('isBuyFish').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('isBuyFish').value = "false";
    	}

	  })
	  
	      document.getElementById("buyBeer").addEventListener("click", function(){
    	if(confirm("Точно купить пиво?")) {
    	   	 document.getElementById('isBuyBeer').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('isBuyBeer').value = "false";
    	}

	  })
	  document.getElementById("buyBeer2").addEventListener("click", function(){
    	if(confirm("Точно купить пиво?")) {
    	   	 document.getElementById('isBuyBeer').value = "true";
    		 document.getElementById('submit').click();
    	} else {
    	   	 document.getElementById('isBuyBeer').value = "false";
    	}

	  })
	  
</script>
</html>