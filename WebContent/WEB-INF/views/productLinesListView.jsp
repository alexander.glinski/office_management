<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>ProductLines List</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>ProductLines List</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>ProductLine</th>
          <th>textDescription</th>
          <th>htmlDescription</th>
          <th>image</th>
          <th>Edit</th>
          <th>Delete</th>
       </tr>
       <c:forEach items="${productLinesList}" var="productLines" >
          <tr>
             <td>${productLines.productLine}</td>
             <td>${productLines.textDescription}</td>
             <td>${productLines.htmlDescription}</td>
             <td>${productLines.image}</td>
             <td>
                <a href="editProductLines?productLine=${productLines.productLine}">Edit</a>
             </td>
             <td>
                <a href="deleteProductLines?productLine=${productLines.productLine}">Delete</a>
             </td>
          </tr>
       </c:forEach>
    </table>
 
    <a href="createProductLines" >Create ProductLines</a>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>