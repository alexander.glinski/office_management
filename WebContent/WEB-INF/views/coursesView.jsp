<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
 <head>
    <meta charset="UTF-8">
    <title>Courses</title>
 </head>
 <body>
 
    <jsp:include page="_header.jsp"></jsp:include>
    <jsp:include page="_menu.jsp"></jsp:include>
 
    <h3>Courses</h3>
 
    <p style="color: red;">${errorString}</p>
 
    <table border="1" cellpadding="5" cellspacing="1" >
       <tr>
          <th>Teacher</th>
          <th>Name of Courses</th>
          <th>Date of Start</th>
          <th>Date of End</th>
          <c:if test="${loginedUser.role.id eq 3}">
                    <th>Записаться</th>
          </c:if>
          <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10 ||loginedUser.role.id eq 2 }">
                 <th>Посмотреть студентов</th>
             </c:if>
       </tr>
       <c:forEach items="${coursesList}" var="course" >
          <tr>
             <td>${course.teacherNumber}</td>
             <td>${course.nameOfCourse}</td>
             <td>${course.dateOfStart}</td>
             <td>${course.dateOfStart}</td>
             <c:if test="${loginedUser.role.id eq 3}">
                <td>
                 <a href="applyForCourses?course=${course.id}&user=${loginedUser.userName}"><input type="button" value="Записаться"></a>
                </td>
             </c:if>
             <c:if test="${loginedUser.role.id eq 1 ||loginedUser.role.id eq 10 ||loginedUser.role.id eq 2 }">
                          <td>
                <a href="showStudents?course=${course.id}"><input type="button" value="Посмотреть студентов"></a>
             </td>
             </c:if>
          </tr>
       </c:forEach>
    </table>
 
    <jsp:include page="_footer.jsp"></jsp:include>
 
 </body>
</html>