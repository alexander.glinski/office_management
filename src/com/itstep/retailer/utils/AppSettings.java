package com.itstep.retailer.utils;

import java.util.Properties;

public class AppSettings {
    
    private static Properties props = null;
    private static AppSettings settings = null;
    
    private AppSettings() {
        props = new Properties();
        try {
        	props.load(AppSettings.class.getClassLoader().getResourceAsStream("app.properties"));
        	} catch (Exception e) {
            System.out.println("Something wrong");
            e.printStackTrace();
        }
        System.out.println("Application Settings have been initialized..");
    }
    
    public static AppSettings getSettings(){
        if(settings == null){
            settings = new AppSettings();
        }
        return settings;
    }
    
   public static Properties getProperties() {
	   return props;
   }
    
    
    public String getPropValue(String key){
        return props.getProperty(key);
        
       
        
    }  

}
