package com.itstep.retailer.jdbc;

public enum Environment {
	
	DEV("jdbc:mysql://localhost:3306/retailer", "root", ""),
	PRE_PROD("jdbc:mysql://localhost:3306/mysql", "root", ""),
	PROD("jdbc:mysql://localhost:3306/shop", "root", "");
	
	private String url;
	private String userName;
	private String password;
	
	private Environment(String url, String userName, String password) {
		this.url = url;
		this.userName = userName;
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	

}
