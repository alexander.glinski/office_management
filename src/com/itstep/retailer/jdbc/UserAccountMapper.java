package com.itstep.retailer.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.itstep.retailer.beans.RetType;
import com.itstep.retailer.beans.Role;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.utils.CryptoUtils;

public class UserAccountMapper extends Mapper {
    
	public UserAccountMapper(Connection conn) {
		super(conn);
	}

	private String sql_insert = "INSERT INTO `retailer`.`USER_ACCOUNT` (`USER_NAME`, `GENDER`, `PASSWORD`, `ROLE_ID`, `EMAIL`,`isEmailConfirmed`, `LAST_UPDATED_TS`, `CREATED_TS`) VALUES (?, ?, ?, ?, ?,?, ?, ?)";
    	
	@Override
	public RetType create(RetType rType) {
	    Connection conn = getConn();
		PreparedStatement pstm = null;
		UserAccount user = (UserAccount)rType;
		try {
			pstm = conn.prepareStatement(sql_insert);
			pstm.setString(1,user.getUserName() );
			pstm.setString(2,user.getGender());
			pstm.setString(3,new CryptoUtils().encrypt("2"+user.getPassword()+"4") );
			pstm.setInt(4,user.getRole().getId() );
			pstm.setString(5,user.getEmail() );
			pstm.setString(6,user.getIsEmailConfirmed());
			pstm.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
			pstm.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBUtils.release(null, null, pstm, conn);
		return user;
	}

	@Override
	public List<RetType> findAllObjs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RetType findByPrimaryKey(RetType objWithKeyValues) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(RetType rType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(RetType rType) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		UserAccount user = new UserAccount("BOB","F","123",Role.NO_PERMISSIONS,"BOB@MAIL.RU","NO");		
		new UserAccountMapper(DBUtils.getConnection()).create(user);		
	}

}
