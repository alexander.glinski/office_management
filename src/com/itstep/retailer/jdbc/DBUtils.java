package com.itstep.retailer.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.itstep.retailer.beans.Employee;
import com.itstep.retailer.beans.Office;
import com.itstep.retailer.beans.Order;
import com.itstep.retailer.beans.Product;
import com.itstep.retailer.beans.ProductLines;
import com.itstep.retailer.beans.Role;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.dao.OfficeDAOImpl;
import com.itstep.retailer.utils.MyUtils;
import com.itstep.retailer.utils.AppSettings;
import com.itstep.retailer.utils.CryptoUtils;



public class DBUtils {
	
	public final static String URL = "jdbc:mysql://localhost:3306/retailer";
	public final static String USER_ID = "root";
	public final static String PASSWORD = "";
	
	public static String coverLast(Object obj) {
		if(obj == null) {
			return "null";
		} else {
			return "'" + obj.toString() + "'";
		}
	}
	
	public static String cover(Object obj) {
		
			return coverLast( obj) + ", ";
		}
	
	public static Connection getConnection(Environment env){
        Connection conn = null;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
            } catch (InstantiationException e) {
                System.out.println(1);
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                System.out.println(2);
                e.printStackTrace();
            } catch (ClassNotFoundException e ) {
                System.out.println(3);
                e.printStackTrace();
            }
            conn = DriverManager.getConnection(env.getUrl(), 
                    env.getUserName(), env.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
	
	public static Connection getConnection () {
		Environment env = null;
		switch (AppSettings.getSettings().getPropValue("env")) {
		case "PROD":
			env = Environment.PROD;
			break;
		case "DEV":
			env = Environment.DEV;
			break;
		case "PRE_PROD":
			env = Environment.PRE_PROD;
			break;

		default:
			break;
		
		}
		return getConnection(env);
	}
	
	public static void release (ResultSet rs, Statement stmt, PreparedStatement pstmt,Connection conn ) {
		if (rs!= null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		if (stmt!= null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		if (pstmt!= null) {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (conn!= null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
	}
	
	public static UserAccount findUser(Connection conn, String userName, String password) throws Exception {
		 
        String sql = "Select a.User_Name, a.Password, a.Gender,a.ROLE_ID,a.EMAIL,a.isEmailConfirmed from User_Account a where a.User_Name = ? and a.password= ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, userName);       
        pstm.setString(2, new CryptoUtils().encrypt(password));
        ResultSet rs = pstm.executeQuery();
 
        if (rs.next()) {
            String gender = rs.getString("Gender");
            int roleId = rs.getInt("ROLE_ID");
            UserAccount user = new UserAccount();
            user.setUserName(userName);
            user.setPassword(password);
            user.setGender(gender);
            user.setRole(Role.getRole(roleId));
            user.setEmail(rs.getString("EMAIL"));
            user.setIsEmailConfirmed(rs.getString("isEmailConfirmed"));
            return user;
        }
        return null;
    }
 
    public static UserAccount findUser(Connection conn, String userName) throws Exception {
 
        String sql = "Select a.User_Name, a.Password, a.Gender,a.ROLE_ID,a.EMAIL,a.isEmailConfirmed from User_Account a "//
                + " where a.User_Name = ? ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, userName);
 
        ResultSet rs = pstm.executeQuery();
 
        if (rs.next()) {
            String password = rs.getString("Password");
            String gender = rs.getString("Gender");
            int roleId = rs.getInt("ROLE_ID");
            UserAccount user = new UserAccount();
            user.setUserName(userName);
            user.setPassword(password);
            user.setGender(gender);
            user.setRole(Role.getRole(roleId));
            user.setEmail(rs.getString("EMAIL"));
            user.setIsEmailConfirmed(rs.getString("isEmailConfirmed"));
            return user;
        }
        return null;
    }
    public static void createUser(Connection conn, UserAccount user) throws Exception {
    	 
    	 String sql = "Insert into USER_ACCOUNT(USER_NAME, GENDER,PASSWORD,ROLE_ID,EMAIL,isEmailConfirmed) values (?,?,?,?,?,?)";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, user.getUserName());
        pstm.setString(2, user.getGender());
        pstm.setString(3, new CryptoUtils().encrypt( user.getPassword()));
        pstm.setInt(4, -1);
        pstm.setString(5, user.getEmail());
        pstm.setString(6, user.getIsEmailConfirmed());
        pstm.executeUpdate();       
    }
 
    public static List<Product> queryProduct(Connection conn) throws SQLException {
        String sql = "Select a.Code, a.Name, a.Price from Product a ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<Product> list = new ArrayList<Product>();
        while (rs.next()) {
            String code = rs.getString("Code");
            String name = rs.getString("Name");
            float price = rs.getFloat("Price");
            Product product = new Product();
            product.setCode(code);
            product.setName(name);
            product.setPrice(price);
            list.add(product);
        }
        return list;
    }
    
    public static List<Employee> queryEmployee(Connection conn) throws SQLException {
        String sql = "Select a.employeeNumber, a.lastName, a.firstName,a.extension,a.email,a.officeCode,a.reportsTo,a.jobTitle,a.LAST_UPDATED from Employees a ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<Employee> list = new ArrayList<Employee>();
        while (rs.next()) {
            int employeeNumber = rs.getInt("employeeNumber");
            String lastName = rs.getString("lastName");
            String firstName = rs.getString("firstName");
            String extension = rs.getString("extension");
            String email = rs.getString("email");
            int officeCode = rs.getInt("officeCode");
            int reportsTo = rs.getInt("reportsTo");
            String jobTitle = rs.getString("jobTitle");
            Timestamp ts = rs.getTimestamp("LAST_UPDATED");
            
            Employee e = new Employee();
            e.setEmployeeNumber(employeeNumber);
            e.setLastName(lastName);
            e.setFirstName(firstName);
            e.setExtension(extension);
            e.setEmail(email);
            e.setOffice(new OfficeDAOImpl().getByKey(officeCode));
            e.setReportsTo(reportsTo);
            e.setJobTitle(jobTitle);
            list.add(e);
        }
        return list;
    }
    
    public static List<ProductLines> queryProductLines(Connection conn) throws SQLException {
        String sql = "Select a.productLine, a.textDescription, a.htmlDescription,a.image from ProductLines a ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<ProductLines> list = new ArrayList<ProductLines>();
        while (rs.next()) {
            String productLine = rs.getString("productLine");
            String textDescription = rs.getString("textDescription");
            String htmlDescription = rs.getString("htmlDescription");
            String image = rs.getString("image");
            ProductLines pl = new ProductLines();
            pl.setProductLine(productLine);;
            pl.setTextDescription(textDescription);
            pl.setHtmlDescription(htmlDescription);
            pl.setImage(image);
            list.add(pl);
        }
        return list;
    }
    
    
    public static ProductLines findProductLines(Connection conn, String productLine) throws SQLException {
        String sql = "Select a.productLine, a.textDescription, a.htmlDescription,a.image from ProductLines a where a.productLine=?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, productLine);
 
        ResultSet rs = pstm.executeQuery();
 
        while (rs.next()) {            
            String textDescription = rs.getString("textDescription");
            String htmlDescription = rs.getString("htmlDescription");
            String image = rs.getString("image");
            ProductLines pl = new ProductLines(productLine,textDescription,htmlDescription,image);
            return pl;
        }
        return null;
    }
 
    public static Product findProduct(Connection conn, String code) throws SQLException {
        String sql = "Select a.Code, a.Name, a.Price from Product a where a.Code=?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, code);
 
        ResultSet rs = pstm.executeQuery();
 
        while (rs.next()) {
            String name = rs.getString("Name");
            float price = rs.getFloat("Price");
            Product product = new Product(code, name, price);
            return product;
        }
        return null;
    }
    
    public static Office findOffice(Connection conn, String officeCode) throws SQLException {
        String sql = "Select a.city, a.phone, a.addressLine1, a.addressLine2, a.state, a.country, a.postalCode, a.territory from Offices a where a.officeCode=?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, officeCode);
 
        ResultSet rs = pstm.executeQuery();
 
        while (rs.next()) {
            String city = rs.getString("city");
            String phone = rs.getString("phone");
            String addressLine1 = rs.getString("addressLine1");
            String addressLine2 = rs.getString("addressLine2");
            String state = rs.getString("state");
            String country = rs.getString("country");
            String postalCode = rs.getString("postalCode");
            String territory = rs.getString("territory");
            Office office = new Office(Integer.parseInt(officeCode),city, phone, addressLine1,addressLine2,state,country,postalCode,territory);
            return office;
        }
        return null;
    }
    
    public static Employee findEmployee(Connection conn, String employeeNumber) throws SQLException {
    	 String sql = "Select a.lastName, a.firstName,a.extension,a.email,a.officeCode,a.reportsTo,a.jobTitle,a.LAST_UPDATED from Employees a Where employeeNumber=?";
    	 
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, employeeNumber);
 
        ResultSet rs = pstm.executeQuery();
 
        while (rs.next()) {
            String lastName = rs.getString("lastName");
            String firstName = rs.getString("firstName");
            String extension = rs.getString("extension");
            String email = rs.getString("email");
            int officeCode = rs.getInt("officeCode");
            int reportsTo = rs.getInt("reportsTo");
            String jobTitle = rs.getString("jobTitle");
            Timestamp ts = rs.getTimestamp("LAST_UPDATED");
            Employee e = new Employee();
            e.setEmployeeNumber(Integer.parseInt(employeeNumber));
            e.setLastName(lastName);
            e.setFirstName(firstName);
            e.setExtension(extension);
            e.setEmail(email);
            e.setOffice(new OfficeDAOImpl().getByKey(officeCode));
            e.setReportsTo(reportsTo);
            e.setJobTitle(jobTitle);
            e.setLAST_UPDATED(ts);
            return e;
        }
        return null;
    }
    
    public static void updateProductLine(Connection conn, ProductLines productLines) throws SQLException {
        String sql = "Update ProductLines set textDescription =?, htmlDescription=?,image=? where productLine=? ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        System.out.println(productLines);
        pstm.setString(1, productLines.getTextDescription());
        pstm.setString(2, productLines.getHtmlDescription());
        pstm.setString(3, productLines.getImage());
        pstm.setString(4, productLines.getProductLine());
        pstm.executeUpdate();
    }
 
    public static void updateProduct(Connection conn, Product product) throws SQLException {
        String sql = "Update Product set Name =?, Price=? where Code=? ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, product.getName());
        pstm.setFloat(2, product.getPrice());
        pstm.setString(3, product.getCode());
        pstm.executeUpdate();
    }
    public static void updateEmployee(Connection conn, Employee employee) throws SQLException {
        String sql = "Update Employees set  lastName=?,firstName =?, extension=?,email =?, officeCode=?,reportsTo =?, jobTitle=?,LAST_UPDATED =? where employeeNumber=? ";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, employee.getLastName());
        pstm.setString(2, employee.getFirstName());
        pstm.setString(3, employee.getExtension());
        pstm.setString(4, employee.getEmail());
        pstm.setInt(5, employee.getOffice().getOfficeCode());
        pstm.setInt(6, employee.getReportsTo());
        pstm.setString(7, employee.getJobTitle());
        pstm.setTimestamp(8, employee.getLAST_UPDATED());
        pstm.setInt(9, employee.getEmployeeNumber());
        pstm.executeUpdate();
    }
 
    public static void insertProduct(Connection conn, Product product) throws SQLException {
        String sql = "Insert into Product(Code, Name,Price) values (?,?,?)";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, product.getCode());
        pstm.setString(2, product.getName());
        pstm.setFloat(3, product.getPrice());
 
        pstm.executeUpdate();
    }
    public static void insertProductLines(Connection conn, ProductLines productLines) throws SQLException {
        String sql = "Insert into ProductLines(productLine, textDescription,htmlDescription,image) values (?,?,?,?)";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, productLines.getProductLine());
        pstm.setString(2, productLines.getTextDescription());
        pstm.setString(3, productLines.getHtmlDescription());
        pstm.setString(4, productLines.getImage());
 
        pstm.executeUpdate();
    }
    
    public static void insertEmployee(Connection conn, Employee employee) throws SQLException {
        String sql = "Insert into Employees(employeeNumber,lastName,firstName, extension,email, officeCode,reportsTo, jobTitle,LAST_UPDATED) values(?,?,?,?,?,?,?,?,?)";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, employee.getEmployeeNumber());
        pstm.setString(2, employee.getLastName());
        pstm.setString(3, employee.getFirstName());
        pstm.setString(4, employee.getExtension());
        pstm.setString(5, employee.getEmail());
        pstm.setInt(6, employee.getOffice().getOfficeCode());
        pstm.setInt(7, employee.getReportsTo());
        pstm.setString(8, employee.getJobTitle());
        pstm.setTimestamp(9, employee.getLAST_UPDATED());
        pstm.executeUpdate();
    }
    
    public static void deleteProductLines(Connection conn, String productLine) throws SQLException {
        String sql = "Delete From ProductLines where productLine= ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, productLine);
 
        pstm.executeUpdate();
    }
 
    public static void deleteProduct(Connection conn, String code) throws SQLException {
        String sql = "Delete From Product where Code= ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, code);
 
        pstm.executeUpdate();
    }
    public static void deleteEmployee(Connection conn, String employeeNumber) throws SQLException {
        String sql = "Delete From Employees where employeeNumber= ?";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        pstm.setString(1, employeeNumber);
 
        pstm.executeUpdate();
    }
    
    public static void saveChangesForEmployee(Connection conn,String employeeNumber,HttpSession session,String reason,String whatToDo) throws SQLException {
    	String sql = "Insert into ChangesFroEmployees(id,WHO_IS_CHANGED, ID_EMPLOYEES,REASON,whatToDo,Date) values (NULL,?,?,?,?,?)";
    	 UserAccount userInSession = MyUtils.getLoginedUser(session);
    	 Date date = new Date(new java.util.Date().getTime());
         if(userInSession!=null) {
             PreparedStatement pstm = conn.prepareStatement(sql);
             pstm.setString(1,userInSession.getUserName());
             pstm.setInt(2,Integer.parseInt(employeeNumber));
             pstm.setString(3,reason);
             pstm.setString(4,whatToDo);
             pstm.setDate(5, date);
             pstm.executeUpdate();            
         } else {
        	 System.out.println("Пользователь не зарегистрирован");
         }

 

    }
    
    public static List<String> addUrlForRole(Connection conn,String role) throws SQLException {
    	List<String> l = new ArrayList<>();
    	String sql = "Select "+role+" from URL_PATTERNS";
    	Statement stm = conn.createStatement();
    	ResultSet rs = stm.executeQuery(sql);
    	while(rs.next()) {
    		String valueOfRow = rs.getString(role);
    		if(valueOfRow.trim().length()!=0) {
        		l.add(valueOfRow);
    		} else break;
    	}
    	return l;
    }
    
    public static void confirmEmail(Connection conn,String userName) throws Exception {
    	String sql = "UPDATE `USER_ACCOUNT` SET `isEmailConfirmed` = 'YES' WHERE `USER_ACCOUNT`.`USER_NAME` = '"+userName+"'";
    	UserAccount user = DBUtils.findUser(conn, userName);
    	if(user.getRole()==Role.NO_PERMISSIONS) {
    		DBUtils.updateToUser(conn, userName);
    	}
    	Statement stm = conn.createStatement();
    	stm.executeUpdate(sql);
    }
    
    public static void updateToUser(Connection conn,String userName) throws SQLException {
    	String sql = "UPDATE `USER_ACCOUNT` SET `ROLE_ID` = '3' WHERE `USER_ACCOUNT`.`USER_NAME` = '"+userName+"'";
    	Statement stm = conn.createStatement();
    	stm.executeUpdate(sql);
    }
	
    public static void buyProduct(Connection conn,String userName,String code) throws SQLException {
    	String sql = "INSERT INTO `ordersForUsers` (`orderNumber`,`productCode`, `orderDate`, `requiredDate`, `shippedDate`, `status`, `USER_NAME`) VALUES (NULL,?, ?, '','' , 'Ждет принятия', '"+userName+"')";
    	PreparedStatement pstm = conn.prepareStatement(sql);
    	pstm.setString(1, code);
    	Date date = new Date(new java.util.Date().getTime());
    	pstm.setDate(2, date);
    	pstm.executeUpdate();
    }
    
    public static List<Order> queryOrders(Connection conn,String USER_NAME) throws SQLException {
        String sql = "Select a.orderNumber, a.productCode, a.orderDate,a.requiredDate, a.shippedDate, a.status,a.USER_NAME from ordersForUsers a  WHERE a.USER_NAME = '" + USER_NAME+"';";
 
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<Order> list = new ArrayList<Order>();
        while (rs.next()) {
            int orderNumber = rs.getInt("orderNumber");
            String productCode = rs.getString("productCode");
            Date orderDate = rs.getDate("orderDate");
            String requiredDate = rs.getString("requiredDate");
            String shippedDate = rs.getString("shippedDate");
            String status = rs.getString("status");
            String userName = rs.getString("USER_NAME");
            Order order = new Order();
            order.setOrderDate(orderDate);
            order.setOrderNumber(orderNumber);
            order.setProductCode(productCode);
            order.setRequiredDate(requiredDate);
            order.setShippedDate(shippedDate);
            order.setStatus(status);
            order.setUserName(userName);
            list.add(order);
        }
        return list;
    }
    public static List<Order> queryOrdersToManagers(Connection conn,String USER_NAME) throws SQLException {
        String sql = "Select a.orderNumber, a.productCode, a.orderDate,a.requiredDate, a.shippedDate, a.status,a.USER_NAME from ordersForUsers a  WHERE a.status = 'Ждет принятия' OR a.status='В обработке'";
        PreparedStatement pstm = conn.prepareStatement(sql);
 
        ResultSet rs = pstm.executeQuery();
        List<Order> list = new ArrayList<Order>();
        while (rs.next()) {
            int orderNumber = rs.getInt("orderNumber");
            String productCode = rs.getString("productCode");
            Date orderDate = rs.getDate("orderDate");
            String requiredDate = rs.getString("requiredDate");
            String shippedDate = rs.getString("shippedDate");
            String status = rs.getString("status");
            String userName = rs.getString("USER_NAME");
            Order order = new Order();
            order.setOrderDate(orderDate);
            order.setOrderNumber(orderNumber);
            order.setProductCode(productCode);
            order.setRequiredDate(requiredDate);
            order.setShippedDate(shippedDate);
            order.setStatus(status);
            order.setUserName(userName);
            list.add(order);
        }
        return list;
    }
    
    public static void takeProduct(Connection conn,int orderNumber) throws Exception {
    	Date date = new Date(new java.util.Date().getTime());
    	String realDate = date.toString();
    	String sql = "UPDATE `ordersForUsers` SET `status` = 'В обработке' WHERE `ordersForUsers`.`orderNumber` = '"+orderNumber+"';";
    	Statement stm = conn.createStatement();
    	stm.executeUpdate(sql);
    	sql = "UPDATE `ordersForUsers` SET `requiredDate` = '"+realDate+"' WHERE `ordersForUsers`.`orderNumber` = '"+orderNumber+"';";
    	stm.executeUpdate(sql);
    }
    public static void shipProduct(Connection conn,int orderNumber) throws Exception {
    	Date date = new Date(new java.util.Date().getTime());
    	String realDate = date.toString();
    	String sql = "UPDATE `ordersForUsers` SET `status` = 'Shipped' WHERE `ordersForUsers`.`orderNumber` = '"+orderNumber+"';";
    	Statement stm = conn.createStatement();
    	stm.executeUpdate(sql);
    	sql = "UPDATE `ordersForUsers` SET `shippedDate` = '"+realDate+"' WHERE `ordersForUsers`.`orderNumber` = '"+orderNumber+"';";
    	stm.executeUpdate(sql);
    }
    
    public static void cancelOrder(Connection conn,int orderNumber) throws Exception {
    	String sql = "UPDATE `ordersForUsers` SET `status` = 'Отменён' WHERE `ordersForUsers`.`orderNumber` = '"+orderNumber+"';";
    	Statement stm = conn.createStatement();
    	stm.executeUpdate(sql);
    }
}
