package com.itstep.retailer.jdbc;

import java.sql.Connection;
import java.util.List;

import com.itstep.retailer.beans.RetType;

public abstract class Mapper {
	
	private Connection conn;
	
    public abstract RetType create(RetType rType);
    public abstract List<RetType> findAllObjs();
    public abstract RetType findByPrimaryKey(RetType objWithKeyValues);
    public abstract void remove(RetType rType);
    public abstract void update(RetType rType);
    
    public Mapper(Connection conn) {
    	this.conn = conn;
    }
	public Connection getConn() {
		return conn;
	}
	public void setConn(Connection conn) {
		this.conn = conn;
	}
    
    
}
