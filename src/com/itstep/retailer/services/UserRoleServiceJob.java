package com.itstep.retailer.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.itstep.retailer.beans.Role;

public class UserRoleServiceJob {
	
	public static Map<Role,List<String>> getRolesUrlsMapping() {
		
		Map<Role,List<String>> roles_urld = new HashMap<>();
		String file = "role_urls.properties";
		InputStream in;
		Properties props = new Properties();
		in = UserRoleServiceJob.class.getClassLoader().getResourceAsStream(file);
		if(in==null) {
			System.out.println("Resource not found");
			return null;
		}
		try {
			props.load(in);
			Enumeration<?> e = props.propertyNames();
			while(e.hasMoreElements()) {
				String roleStr = (String) e.nextElement();
				String urls_str = props.getProperty(roleStr);				
				roles_urld.put(Role.valueOf(roleStr),Arrays.asList( urls_str.split(";")));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return roles_urld;
	}
	public static void main(String[] args) {
		 UserRoleServiceJob.getRolesUrlsMapping();
	}
}
