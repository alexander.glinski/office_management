package com.itstep.retailer.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.itstep.retailer.beans.Role;
import com.itstep.retailer.jdbc.DBUtils;


public class MappingUrl {
	
	public MappingUrl() {
	}


	public  Map<Role,List<String>>  setRolesPatterns() throws SQLException {
		 Map<Role, List<String>> map = new HashMap<Role, List<String>>();
		Connection conn = DBUtils.getConnection();
		map.put(Role.ADMIN, DBUtils.addUrlForRole(conn, "ADMIN"));
		map.put(Role.MANAGER,  DBUtils.addUrlForRole(conn, "MANAGER"));
		map.put(Role.USER,  DBUtils.addUrlForRole(conn, "USER"));     
		map.put(Role.SUPER_USER,  DBUtils.addUrlForRole(conn, "SUPER_USER"));
		map.put(Role.NO_PERMISSIONS,  DBUtils.addUrlForRole(conn, "NO_PERMISSIONS"));
		return map;
	}
	
	

}
