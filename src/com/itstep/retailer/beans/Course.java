package com.itstep.retailer.beans;

import java.sql.Timestamp;

public class Course {
	
	private int id;
	private int teacherNumber;
	private String nameOfCourse;
	private Timestamp dateOfStart;
	private Timestamp dateOfEnd;
	private String status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTeacherNumber() {
		return teacherNumber;
	}
	public void setTeacherNumber(int teacherNumber) {
		this.teacherNumber = teacherNumber;
	}
	public String getNameOfCourse() {
		return nameOfCourse;
	}
	public void setNameOfCourse(String nameOfCourse) {
		this.nameOfCourse = nameOfCourse;
	}
	public Timestamp getDateOfStart() {
		return dateOfStart;
	}
	public void setDateOfStart(Timestamp dateOfStart) {
		this.dateOfStart = dateOfStart;
	}
	public Timestamp getDateOfEnd() {
		return dateOfEnd;
	}
	public void setDateOfEnd(Timestamp dateOfEnd) {
		this.dateOfEnd = dateOfEnd;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
