package com.itstep.retailer.beans;

public enum Role {
	
	ADMIN(1),MANAGER(2),USER(3),SUPER_USER(10),NO_PERMISSIONS(-1);	
	private int id;
	
	private Role(int id) {
		this.id=id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public static Role getRole(int id) {
		switch (id) {
		case 1: return Role.ADMIN;
		case 2: return Role.MANAGER;
		case 3: return Role.USER;
		case 10: return Role.SUPER_USER;
		case -1: return Role.NO_PERMISSIONS;
			

		default:
			throw new IllegalArgumentException("non-existind role id=" + id);
		}
	}
}
