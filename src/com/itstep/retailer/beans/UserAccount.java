package com.itstep.retailer.beans;

import java.sql.Timestamp;

public class UserAccount extends RetType {
	public static final String GENDER_MALE = "M";
	public static final String GENDER_FEMALE = "F";

	private String userName;
	private String gender;
	private String password;
	private Role role;
	private String email;
	private String isEmailConfirmed;
	private Timestamp lastUpdatedTime;
	private Timestamp createdTime;	
	
	
	
	
	public UserAccount(String userName, String gender, String password, Role role, String email,
			String isEmailConfirmed) {
		super();
		this.userName = userName;
		this.gender = gender;
		this.password = password;
		this.role = role;
		this.email = email;
		this.isEmailConfirmed = isEmailConfirmed;
	}

	public Timestamp getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Timestamp lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Timestamp getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIsEmailConfirmed() {
		return isEmailConfirmed;
	}

	public void setIsEmailConfirmed(String isEmailConfirmed) {
		this.isEmailConfirmed = isEmailConfirmed;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}


	public UserAccount() {

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
