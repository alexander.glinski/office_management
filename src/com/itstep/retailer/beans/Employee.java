package com.itstep.retailer.beans;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Employees")
public class Employee {

  @Id
  @Column(name = "employeeNumber")
  public int employeeNumber;

  public String lastName;
  public String firstName;
  public String extension;
  public String email;
  private Timestamp LAST_UPDATED;
  // @OneToOne(fetch = FetchType.LAZY, mappedBy = "Employees")
  // @Column(name = "officeCode")
  // public int officeCode;
  //
  @ManyToOne
  @JoinColumn(name = "officeCode")
  private Office office;

  public int reportsTo;
  public String jobTitle;

  public Employee() {

  }

  public Office getOffice() {
    return office;
  }

  public void setOffice(Office office) {
    this.office = office;
  }

  public Timestamp getLAST_UPDATED() {
    return LAST_UPDATED;
  }

  public void setLAST_UPDATED(Timestamp lAST_UPDATED) {
    LAST_UPDATED = lAST_UPDATED;
  }

  public Employee(String lastName, String firstName, String extension, String email, int reportsTo,
      String jobTitle) {
    super();
    this.lastName = lastName;
    this.firstName = firstName;
    this.extension = extension;
    this.email = email;
    this.reportsTo = reportsTo;
    this.jobTitle = jobTitle;
  }

  public Employee(int employeeNumber, String lastName, String firstName, String extension,
      String email, int reportsTo, String jobTitle) {
    super();
    this.employeeNumber = employeeNumber;
    this.lastName = lastName;
    this.firstName = firstName;
    this.extension = extension;
    this.email = email;
    // this.officeCode = officeCode;
    this.reportsTo = reportsTo;
    this.jobTitle = jobTitle;
  }

  public int getEmployeeNumber() {
    return employeeNumber;
  }

  public void setEmployeeNumber(int employeeNumber) {
    this.employeeNumber = employeeNumber;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  // public int getOfficeCode() {
  // return officeCode;
  // }
  // public void setOfficeCode(int officeCode) {
  // this.officeCode = officeCode;
  // }
  public int getReportsTo() {
    return reportsTo;
  }

  public void setReportsTo(int reportsTo) {
    this.reportsTo = reportsTo;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }



  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + employeeNumber;
    result = prime * result + ((extension == null) ? 0 : extension.hashCode());
    result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
    result = prime * result + ((jobTitle == null) ? 0 : jobTitle.hashCode());
    result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
    result = prime * result + ((office == null) ? 0 : office.hashCode());
    result = prime * result + reportsTo;
    return result;
  }



  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Employee other = (Employee) obj;
    if (email == null) {
      if (other.email != null)
        return false;
    } else if (!email.equals(other.email))
      return false;
    if (employeeNumber != other.employeeNumber)
      return false;
    if (extension == null) {
      if (other.extension != null)
        return false;
    } else if (!extension.equals(other.extension))
      return false;
    if (firstName == null) {
      if (other.firstName != null)
        return false;
    } else if (!firstName.equals(other.firstName))
      return false;
    if (jobTitle == null) {
      if (other.jobTitle != null)
        return false;
    } else if (!jobTitle.equals(other.jobTitle))
      return false;
    if (lastName == null) {
      if (other.lastName != null)
        return false;
    } else if (!lastName.equals(other.lastName))
      return false;
    if (office == null) {
      if (other.office != null)
        return false;
    } else if (!office.equals(other.office))
      return false;
    if (reportsTo != other.reportsTo)
      return false;
    return true;
  }



}
