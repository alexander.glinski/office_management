package com.itstep.retailer.beans;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LocaleChanger implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	
	public LocaleChanger() {
		// TODO Auto-generated constructor stub
	}
	
	public void changeLocale(String localeCode) {
		currentLocale = new Locale(localeCode);
	}

	public Locale getCurrentLocale() {
		return currentLocale;
	}

	public void setCurrentLocale(Locale currentLocale) {
		this.currentLocale = currentLocale;
	}
	
	

}
