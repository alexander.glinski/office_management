package com.itstep.retailer.dao;

import java.util.List;

import com.itstep.retailer.beans.Employee;

public interface EmployeeDAO {
	List<Employee> findByOfficeCode(int officeCode);
	List<Employee> findAllOffices();
	Employee findByCode(int employeeNumber);
}
