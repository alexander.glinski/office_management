package com.itstep.retailer.dao;

import java.util.List;

import com.itstep.retailer.beans.Office;

public class OfficeDAOImpl extends AbstractDAO<Integer, Office> implements OfficeDAO {

	@Override
	public List<Office> findByCity(String city) {
		return getSession().createQuery("SELECT o FROM Office o WHERE o.city='" + city + "'", Office.class).list();
	}

	@Override
	public List<Office> findAllOffices() {
		return getSession().createQuery("SELECT o FROM Office o", Office.class).list();
	}

	@Override
	public Office findByCode(int officeCode) {
		return getByKey(officeCode);
	}

}
