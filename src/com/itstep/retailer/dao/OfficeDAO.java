package com.itstep.retailer.dao;

import java.util.List;

import com.itstep.retailer.beans.Office;

public interface OfficeDAO {
	
	List<Office> findByCity(String city);
	List<Office> findAllOffices();
	Office findByCode(int officeCode);
}
