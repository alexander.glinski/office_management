package com.itstep.retailer.dao;

import java.util.List;

import com.itstep.retailer.beans.ProductLines;

public interface ProductLinesDAO {
	
	List<ProductLines> findAll();

}
