package com.itstep.retailer.dao;

import java.util.List;

import com.itstep.retailer.beans.ProductLines;

public class ProductLinesDAOImpl extends AbstractDAO<Integer, ProductLines> implements ProductLinesDAO {

	@Override
	public List<ProductLines> findAll() {
		return getSession().createQuery("SELECT pl FROM ProductLines pl", ProductLines.class).list();
	}
	

}
