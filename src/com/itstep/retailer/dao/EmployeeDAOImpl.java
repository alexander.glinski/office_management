package com.itstep.retailer.dao;

import java.util.List;

import com.itstep.retailer.beans.Employee;

public class EmployeeDAOImpl extends  AbstractDAO<Integer, Employee>  implements EmployeeDAO {

	@Override
	public List<Employee> findByOfficeCode(int officeCode) {
		return getSession().createQuery("SELECT o FROM Employee o WHERE o.officeCode='" + officeCode + "'", Employee.class).list();
	}

	@Override
	public List<Employee> findAllOffices() {
		return getSession().createQuery("SELECT o FROM Employee o", Employee.class).list();
	}

	@Override
	public Employee findByCode(int employeeNumber) {
		return getByKey(employeeNumber);
	}

}
