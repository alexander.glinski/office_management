package com.itstep.retailer.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import org.hibernate.Session;

import com.itstep.retailer.beans.Office;
import com.itstep.retailer.utils.HibernateUtil;

public abstract class AbstractDAO<K extends Serializable, T> {
	
	private Class<T> persistentClass;
	@SuppressWarnings("unchecked")
	public AbstractDAO() {
        persistentClass = (Class<T>) ((ParameterizedType)
                this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }
	
	protected Session getSession() {
		return HibernateUtil.getSessionFactory().openSession();
	}
	
	public T getByKey(K key) {
		return getSession().get(persistentClass, key);
	}
	
	public void delete(T entity) {
		getSession().remove(entity);
	}
	
	public void update(T entity) {
		getSession().update(entity);
	}
	
	public void create(T entity) {
		getSession().persist(entity);
	}
      
}
