package com.itstep.retailer.filter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.itstep.retailer.beans.Role;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.services.MappingUrl;
import com.itstep.retailer.utils.MyUtils;

@WebFilter(filterName = "rolesCheckerFilter", urlPatterns = { "/*" })
public class RolesCheckerFilter implements Filter {

	private static Map<Role, List<String>> map;

	public RolesCheckerFilter() {

	}

	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("RoleChecked Filter");
        if(!(request instanceof HttpServletRequest)) {
        	throw new ServletException("Our sistem not supported this tipe of request");
        } 
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();
		UserAccount userInSession = MyUtils.getLoginedUser(session);
		String url = req.getServletPath();

		RequestDispatcher rd = null;
		boolean flag = true;
		if (userInSession != null) {

			Role roleUser = userInSession.getRole();
			if (map.get(roleUser).contains(url) || map.get(roleUser).contains("/*")) {
				chain.doFilter(request, response);
			} else {
				rd = req.getRequestDispatcher("/WEB-INF/views/noFunctionalityAvailable.jsp");
				rd.forward(request, response);

			}

		} else {
			flag = true;
			if (map.get(Role.NO_PERMISSIONS).contains(url)) {
				flag = false;
				chain.doFilter(request, response);
			}
			if (flag) {
				rd = req.getRequestDispatcher("/WEB-INF/views/loginView.jsp");
				rd.forward(req, response);
			}

		}

	}

	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("RoleChekedFilter initialization");
		MappingUrl mappingUrl = new MappingUrl();
		try {
			map = mappingUrl.setRolesPatterns();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
