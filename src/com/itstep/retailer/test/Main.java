package com.itstep.retailer.test;

import java.util.Arrays;

import org.hibernate.Session;

import com.itstep.retailer.beans.Employee;
import com.itstep.retailer.beans.Office;
import com.itstep.retailer.dao.OfficeDAOImpl;
import com.itstep.retailer.utils.HibernateUtil;

public class Main {

	public static void main(String[] args) {
		
		
	/*	Office officeToAdd = new Office(55,"Vitebsk","+375275385637","ul.Stroiteleu","d.23","","BLR","536353","42235");
		
		session.beginTransaction();
		session.saveOrUpdate(officeToAdd);
		
		session.getTransaction().commit();
		//HibernateUtil.getSessionFactory().close();
		session.close();
		
		//New
		
		OfficeDAOImpl impl = new OfficeDAOImpl();
		
		System.out.println(impl.findByCode(5).toString());
		*/
		
/*		session.beginTransaction();
		String sql = "select version()";
		
		session.getTransaction().commit();
		session.close();*/
		//HibernateUtil.shutdown();
		
        Session session = HibernateUtil.getSessionFactory().openSession();
//      Office o = session.get(Office.class, 10);
//      List<Employee> list = o.getEmployees();
//      System.out.println(list.size());
//      session.close();
       
        Office office = 
                new Office(167,"Vitebsk", "+375454541258", "ul. Stroitelei 23", "ul. Stroitelei 23", "", "BLR", "65465", "651561");
        Employee epmpl1 = new Employee(3653,"Ivan1", "Ivan1", "asdddd", "Ivan1@gmail.com", 1002, "Developer");
        Employee epmpl2 = new Employee(3654,"Ivan2", "Ivan2", "asdddd", "Ivan2@gmail.com", 1002, "Developer");
        office.setEmployees(Arrays.asList(epmpl1, epmpl2));
        session.beginTransaction();
      
        session.save(office);
        session.save(epmpl1);
        session.save(epmpl2);
        
        session.getTransaction().commit();
        session.close();
		

	}

}
