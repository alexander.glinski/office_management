package com.itstep.retailer.test;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.itstep.retailer.dao.ProductLinesDAOImpl;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.HibernateUtil;

public class ProductLinesTestPerformance {
	
	static {
        HibernateUtil.getSessionFactory();
	}


    @Before
    public void initTest() {
        System.out.println("Тест начат");
    }

    @After
    public void afterTest() {
        System.out.println("Тесты закончены");
        HibernateUtil.shutdown();
    }

    @Test
    public void testQueryProductLinesJDBC() throws Exception {
    	Connection conn = DBUtils.getConnection();
    	for (int i = 0; i < 10000; i++) {
        	try {
    			DBUtils.queryProductLines(conn);
    		} catch (SQLException e) {
    			e.printStackTrace();
    		}
		}
    }
    
    @Test
    public void testQueryProductLinesHibernate() throws Exception {
    	ProductLinesDAOImpl productLinesDAOImpl = new ProductLinesDAOImpl();
    	for (int i = 0; i < 10000; i++) {
    		productLinesDAOImpl.findAll();
		}
    }
    
    
}