package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.utils.MyUtils;


@WebServlet("/deleteStudent")
public class DeleteStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public DeleteStudentServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String course = request.getParameter("course");
		Connection conn = MyUtils.getStoredConnection(request);
		  String sql = "Delete From Students where id= ?";		  
	         
	        try {
	        	PreparedStatement pstm = conn.prepareStatement(sql);	 
		        pstm.setString(1, id);	
				pstm.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	        response.sendRedirect("http:/localhost:8080/app/showStudents?course="+course +"");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
