package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.utils.MyUtils;


@WebServlet("/applStudent")
public class ApplStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ApplStudentServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String course = request.getParameter("course");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String surName = request.getParameter("surName");
		String email = request.getParameter("email");
		String age = request.getParameter("age");
		String sql = "Delete From ApplyStudents where id= "+id+"";
		Connection conn = MyUtils.getStoredConnection(request);
		Statement stm;
		 
		try {
			stm  = conn.createStatement();
			stm.executeUpdate(sql);
			 sql = "INSERT INTO `Students` (`id`, `Course`, `name`, `surName`, `age`, `email`,`Mark`) VALUES (NULL, ?, ?, ?, ?, ?,0)";
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, course);
			pstm.setString(2, name);
			pstm.setString(3, surName);
			pstm.setString(4, age);
			pstm.setString(5, email);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.sendRedirect("http://localhost:8080/app/applyStudents?course=" +course);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
