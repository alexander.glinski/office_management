package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.itstep.retailer.beans.ProductLines;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.MyUtils;

@WebServlet("/productLinesList")
public class ProductLinesListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductLinesListServlet() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 Connection conn = MyUtils.getStoredConnection(request);
		 
	        String errorString = null;
	        List<ProductLines> list = null;
	        try {
	            list = DBUtils.queryProductLines(conn);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            errorString = e.getMessage();
	        }
	        request.setAttribute("errorString", errorString);
	        request.setAttribute("productLinesList", list);
	 
	        RequestDispatcher dispatcher = request.getServletContext()
	                .getRequestDispatcher("/WEB-INF/views/productLinesListView.jsp");
	        dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
