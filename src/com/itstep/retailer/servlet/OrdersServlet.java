package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itstep.retailer.beans.Order;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.MyUtils;


@WebServlet("/orders")
public class OrdersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public OrdersServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

	    UserAccount loginedUser = MyUtils.getLoginedUser(session);
	    Connection conn = MyUtils.getStoredConnection(request);
		List<Order> list = null;
        try {
            list = DBUtils.queryOrdersToManagers(conn,loginedUser.getUserName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("orderList", list);

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/ordersView.jsp");
        dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
