package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itstep.retailer.beans.Course;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.utils.MyUtils;


@WebServlet("/myCourses")
public class MyCoursesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MyCoursesServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Connection conn = MyUtils.getStoredConnection(request);
		UserAccount loginedUser = MyUtils.getLoginedUser(session);
		String errorString = null;
		List<Course> list = new ArrayList<>();
		String sql = "Select * from Course Where Course.teacherNumber = '"+loginedUser.getRole().getId()+"' ";

		PreparedStatement pstm;
		try {
			pstm = conn.prepareStatement(sql);
			ResultSet rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int teacherNumber = rs.getInt("teacherNumber");
				String nameOfCourse = rs.getString("nameOfCourse");
				String status = rs.getString("status");
				Timestamp dateOfStart = rs.getTimestamp("dateOfStart");
				Timestamp dateOfEnd = rs.getTimestamp("dateOfEnd");
				Course course = new Course();
				course.setDateOfEnd(dateOfEnd);
				course.setDateOfStart(dateOfStart);
				course.setNameOfCourse(nameOfCourse);
				course.setId(id);
				course.setStatus(status);
				course.setTeacherNumber(teacherNumber);
				list.add(course);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
        request.setAttribute("loginedUser",loginedUser);
		request.setAttribute("errorString", errorString);
		request.setAttribute("coursesList", list);

		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/myCoursesView.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
