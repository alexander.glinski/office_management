package com.itstep.retailer.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itstep.retailer.beans.Employee;
import com.itstep.retailer.beans.Office;
import com.itstep.retailer.dao.EmployeeDAOImpl;
import com.itstep.retailer.dao.OfficeDAOImpl;


@WebServlet("/viewOffice")
public class ViewOfficeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ViewOfficeServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String employeeNumber = (String)request.getParameter("employeeNumber");
		
		Office office = null;

		String errorString = null;
		Employee e  = new Employee();
		try {
		   e = new EmployeeDAOImpl().getByKey(Integer.parseInt(employeeNumber));
			
		} catch (Exception ex) {
		  ex.printStackTrace();
			errorString = ex.getMessage();
		}

		if (errorString != null && office == null) {
			response.sendRedirect(request.getServletPath() + "/employeeList");
			return;
		}

		request.setAttribute("errorString", errorString);
		request.setAttribute("office", e.getOffice());

		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/views/officeView.jsp");
		dispatcher.forward(request, response);
		
	}
	


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
	}

}
