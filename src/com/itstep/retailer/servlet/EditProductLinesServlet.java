package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itstep.retailer.beans.ProductLines;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.MyUtils;

/**
 * Servlet implementation class EditProductLinesServlet
 */
@WebServlet("/editProductLines")
public class EditProductLinesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditProductLinesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		String productLine = (String) request.getParameter("productLine");

		ProductLines pl = null;

		String errorString = null;

		try {
			pl = DBUtils.findProductLines(conn, productLine);
		} catch (SQLException e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}

		if (errorString != null && pl == null) {
			response.sendRedirect(request.getServletPath() + "/productLinesList");
			return;
		}

		request.setAttribute("errorString", errorString);
		request.setAttribute("productLines", pl);

		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/views/editProductLinesView.jsp");
		dispatcher.forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);
        String productLine = request.getParameter("productLine");
        String textDescription = request.getParameter("textDescription");
        String htmlDescription = request.getParameter("htmlDescription");
        String image = request.getParameter("image");
		ProductLines pl = new ProductLines(productLine,textDescription, htmlDescription, image);

		String errorString = null;

		try {
			DBUtils.updateProductLine(conn, pl);
		} catch (SQLException e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		request.setAttribute("errorString", errorString);
		request.setAttribute("productLines", pl);

		if (errorString != null) {
			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/views/editProductLinesView.jsp");
			dispatcher.forward(request, response);
		}

		else {
			response.sendRedirect(request.getContextPath() + "/productLinesList");
		}
	}

}
