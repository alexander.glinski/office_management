package com.itstep.retailer.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ShopServlet
 */
@WebServlet("/shop")
public class ShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       


    public ShopServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 Cookie[] cookies = request.getCookies();
		 boolean  isBuyMeat = false;
		 boolean  isBuyFish = false;
		 boolean isBuyBeer = false;
		 if (cookies != null) {
	            for (Cookie cookie : cookies) {
	                if ("isBuyMeat".equals(cookie.getName())) {
	                    if("true".equals(cookie.getValue())) {
	                    	isBuyMeat = true;
	                    } 
	                }
	                if ("isBuyFish".equals(cookie.getName())) {
	                    if("true".equals(cookie.getValue())) {
	                    	isBuyFish = true;
	                    } 
	                }
	                if ("isBuyBeer".equals(cookie.getName())) {
	                    if("true".equals(cookie.getValue())) {
	                    	isBuyBeer = true;
	                    } 
	                }
	            }
	        }
		 System.out.println(isBuyMeat);
		 System.out.println(isBuyFish);
		 System.out.println(isBuyBeer);
		 if(isBuyMeat) {
			 request.setAttribute("discountForMeat", ""); 
		 } else {
			 request.setAttribute("discountForMeat", "style=\"display: none;\"");
		 }
		 if(isBuyFish) {
			 request.setAttribute("discountForFish", ""); 
		 } else {
			 request.setAttribute("discountForFish", "style=\"display: none;\"");
		 }
		 if(isBuyBeer) {
			 request.setAttribute("discountForBeer", ""); 
		 } else {
			 request.setAttribute("discountForBeer", "style=\"display: none;\"");
		 }
		 RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/shopView.jsp");
		 rd.forward(request, response);
	
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
