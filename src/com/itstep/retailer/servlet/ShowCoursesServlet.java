package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.beans.Course;
import com.itstep.retailer.utils.MyUtils;

@WebServlet("/showCourses")
public class ShowCoursesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ShowCoursesServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		String errorString = null;
		List<Course> list = new ArrayList<>();
		String sql = "Select * from Course Where Course.status = 'Идет набор' ";

		PreparedStatement pstm;
		try {
			pstm = conn.prepareStatement(sql);
			ResultSet rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int teacherNumber = rs.getInt("teacherNumber");
				String nameOfCourse = rs.getString("nameOfCourse");
				String status = rs.getString("status");
				Timestamp dateOfStart = rs.getTimestamp("dateOfStart");
				Timestamp dateOfEnd = rs.getTimestamp("dateOfEnd");
				Course course = new Course();
				course.setDateOfEnd(dateOfEnd);
				course.setDateOfStart(dateOfStart);
				course.setNameOfCourse(nameOfCourse);
				course.setId(id);
				course.setStatus(status);
				course.setTeacherNumber(teacherNumber);
				list.add(course);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		request.setAttribute("errorString", errorString);
		request.setAttribute("coursesList", list);

		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/coursesView.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
