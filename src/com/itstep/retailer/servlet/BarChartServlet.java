package com.itstep.retailer.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/barChart")
public class BarChartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public BarChartServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/barChartView.jsp");
		rd.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String valuesOfBarChart = request.getParameter("valuesOfBarChart");
		if(valuesOfBarChart!= null) {
			valuesOfBarChart = valuesOfBarChart.replaceAll("  ", " ");
			valuesOfBarChart = valuesOfBarChart.replaceAll("   ", " ");
			valuesOfBarChart = valuesOfBarChart.replaceAll("    ", " ");
			String[] values = valuesOfBarChart.split(" ");
			String errorString = "";
			for (String el : values) {
				if(!el.matches("[-+]?\\d+")) {
					errorString = "Вы ввели неккоретные данные ";
					break;
				}		
			}
			if(errorString.equals("")) {
				request.setAttribute("values",values);
				request.setAttribute("valuesLength", values.length);
			} 
			request.setAttribute("errorString", errorString);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/barChartView.jsp");
		rd.forward(request, response);
	}

}
