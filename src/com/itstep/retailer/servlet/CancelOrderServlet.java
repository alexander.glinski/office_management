package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.conn.MySQLConnUtils;
import com.itstep.retailer.jdbc.DBUtils;


@WebServlet("/cancelOrder")
public class CancelOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CancelOrderServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderNumber = request.getParameter("orderNumber");
		Connection conn;
		try {
			if(orderNumber==null ) {
								
			} else {
				conn = MySQLConnUtils.getMySQLConnection();
				DBUtils.cancelOrder(conn, Integer.parseInt(orderNumber));
			}		 
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect("http://localhost:8080/app/userInfo");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
