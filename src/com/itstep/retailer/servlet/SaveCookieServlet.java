package com.itstep.retailer.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SaveCookieServlet
 */
@WebServlet("/saveCookieServlet")
public class SaveCookieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveCookieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String isBuyMeat = request.getParameter("isBuyMeat");
		String isBuyFish = request.getParameter("isBuyFish");
		String isBuyBeer = request.getParameter("isBuyBeer");
		 Cookie isBuyMeatCookie = null;
		if(isBuyMeat!=null ) {
			if(isBuyMeat.equals("true")) {
				  isBuyMeatCookie = new Cookie("isBuyMeat", "true");

			} else {
				 isBuyMeatCookie = new Cookie("isBuyMeat", "false");
			}
			 isBuyMeatCookie.setMaxAge(24 * 60 * 60);
		     response.addCookie(isBuyMeatCookie);
		}
		Cookie isBuyFishCookie=null;
		if(isBuyFish!=null ) {
			if(isBuyFish.equals("true")) {
				 isBuyFishCookie = new Cookie("isBuyFish", "true");

			} else {
				isBuyFishCookie = new Cookie("isBuyFish", "false");
			}
			 isBuyFishCookie.setMaxAge(24 * 60 * 60);
		     response.addCookie(isBuyFishCookie);
		}
		Cookie isBuyBeerCookie=null;
		if(isBuyBeer!=null) {
			if(isBuyBeer.equals("true")) {
				  isBuyBeerCookie = new Cookie("isBuyBeer", "true");

			} else {
				isBuyBeerCookie = new Cookie("isBuyBeer", "false");
			}
			 isBuyBeerCookie.setMaxAge(24 * 60 * 60);
		     response.addCookie(isBuyBeerCookie);
		}
		RequestDispatcher rd = request.getRequestDispatcher("/shop");
		rd.forward(request, response);
	}

}
