package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.AppSettings;
import com.itstep.retailer.utils.CryptoUtils;
import com.itstep.retailer.utils.MailUtils;

/**
 * Servlet implementation class SendEmailServlet
 */
@WebServlet("/sendEmail")
public class SendEmailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public SendEmailServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String userName = request.getParameter("userName");
		Connection conn = DBUtils.getConnection();
		UserAccount userInSession=null;
		try {
			userInSession = DBUtils.findUser(conn, userName);
		} catch (Exception e1) {			
			e1.printStackTrace();
		}
		System.out.println(userInSession.getEmail()); 
		if(userInSession==null || userInSession.getIsEmailConfirmed().equals("YES")) {
			response.sendRedirect("/WEB-INF/views/loginView.jsp");
		} else {
			try {
				request.setAttribute("user", userInSession); 
				request.setAttribute("loginedUser", userInSession);
		        String fromEmail = AppSettings.getSettings().getPropValue("mailUser");
		        String pass = AppSettings.getSettings().getPropValue("mailPass");
				CryptoUtils cu = new CryptoUtils();
				String strToEnctypt = "2"+userInSession.getPassword().toString()+"4";
				String key = cu.encrypt(strToEnctypt);
				String link = "http://localhost:8080/app/confirmEmail?userName="+userInSession.getUserName()+"&"+"key="+key;
		
				String message = "Hello " + userInSession.getUserName() +"! Вы зарегистрировались на нашем сайте! Чтобы подтвердить вашу регистрацию, нажмите на данную <a href=\""+link+"\">ссылку</a> ";
				String subject = "Confirm registration in App";
				MailUtils.sendEmailWithAttachment(fromEmail, pass, userInSession.getEmail(), subject, message,null);
				request.setAttribute("isEmailSend", "was Send");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
				rd.forward(request, response);
			} catch (Exception e) {
				request.setAttribute("isEmailSend", "not Send");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
				rd.forward(request, response);
				e.printStackTrace();
			}
			
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
