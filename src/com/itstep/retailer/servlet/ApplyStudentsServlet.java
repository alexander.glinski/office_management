package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.itstep.retailer.beans.Student;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.utils.MyUtils;


@WebServlet("/applyStudents")
public class ApplyStudentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ApplyStudentsServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String courses = request.getParameter("course");
		Connection conn = MyUtils.getStoredConnection(request);
		UserAccount loginedUser = MyUtils.getLoginedUser(session);
		String errorString = null;
		List<Student> list = new ArrayList<>();
		String sql = "Select * from ApplyStudents Where ApplyStudents.course = '"+courses+"' ";

		PreparedStatement pstm;
		try {
			pstm = conn.prepareStatement(sql);
			ResultSet rs = pstm.executeQuery();
			while (rs.next()) {
				Student s = new Student();
				int id  = rs.getInt("id");
				int course  = rs.getInt("course");
				int age  = rs.getInt("AGE");
				String name  = rs.getString("STUDENT_NAME");
				String surName  = rs.getString("STUDENT_SURNAME");
				String email  = rs.getString("email");
				s.setAge(age);
				s.setCourse(course);
				s.setEmail(email);
				s.setId(id);
				s.setName(name);
				s.setSurName(surName);
				list.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		request.setAttribute("errorString", errorString);
		request.setAttribute("studentsList", list);
		request.setAttribute("loginedUser",loginedUser);
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/applyStudentsView.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
