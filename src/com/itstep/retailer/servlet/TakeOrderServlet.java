package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.conn.MySQLConnUtils;
import com.itstep.retailer.jdbc.DBUtils;


@WebServlet("/takeOrder")
public class TakeOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public TakeOrderServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderNumber = request.getParameter("orderNumber");
		Connection conn;
		try {
			if(orderNumber==null ) {
								
			} else {
				conn = MySQLConnUtils.getMySQLConnection();
				DBUtils.takeProduct(conn, Integer.parseInt(orderNumber));
			}		 
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect("http://localhost:8080/app/orders");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
