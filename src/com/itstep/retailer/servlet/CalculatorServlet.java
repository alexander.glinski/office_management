package com.itstep.retailer.servlet;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class calculatorServlet
 */
@WebServlet("/calculator")
public class CalculatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalculatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        
		String amountOfTrue = request.getParameter("amountOfTrue");
		String amount = request.getParameter("amount");
		String thePercentageOfCorrect = request.getParameter("thePercentageOfCorrect");
		String answer = request.getParameter("answer");
		String resultOfLast = request.getParameter("result");
		String isEndTasks = request.getParameter("isEndTasks");
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/calculatorView.jsp");
		String nextTask = request.getParameter("isNextTask");
		if(isEndTasks!=null) {
			if(isEndTasks.equals("true")) {
				rd = request.getRequestDispatcher("/WEB-INF/views/resultsTest.jsp");
				request.setAttribute("thePercentageOfCorrect", thePercentageOfCorrect);
				int mark = (int) Double.parseDouble(thePercentageOfCorrect);
				if(mark<=25) {
					request.setAttribute("mark", "Неудовлетворительно");
				} else if(mark<=50) {
					request.setAttribute("mark", "Удовлетворительно");
				} else if(mark<=75) {
					request.setAttribute("mark", "Хорошо");				
				} else {
					request.setAttribute("mark", "Отлично");		
				}
				System.out.println(request.getAttribute("mark"));
			}
		}
		
		if(amountOfTrue==null || amount==null || thePercentageOfCorrect==null) {
			amountOfTrue = "0";
			amount = "0";
			thePercentageOfCorrect="0";
			request.setAttribute("message", "Здравствуйте! Отвечайте на вопрос");
		} else {
			int amountOfTrueNumber = Integer.parseInt(amountOfTrue);
			int amountNumber = Integer.parseInt(amount);
			if(nextTask.equals("true")) {
				amountNumber++;
				request.setAttribute("message", "Следующий вопрос");
			} else {
				if(answer!= null) {

					if(answer.equals(resultOfLast)) {
						amountOfTrueNumber++;
						amountNumber++;
						request.setAttribute("message", "Правильно! Следующий вопрос");
					} else {
						request.setAttribute("message", "Неправильно! Следующий вопрос");
						amountNumber++;
					}
				}
			}
			System.out.println();
			amountOfTrue = amountOfTrueNumber+"";
			amount = amountNumber+"";
			double thePercentageOfCorrectNumber;
			if(amountNumber==0) {
				thePercentageOfCorrectNumber=0;
			} else {
				 thePercentageOfCorrectNumber = amountOfTrueNumber*100/amountNumber;
			}

			thePercentageOfCorrect = thePercentageOfCorrectNumber+"";
			
			
		}
		Random r = new Random();
		int first = r.nextInt(99)+1;
		int second = r.nextInt(99)+1;
		int result;	
		switch (r.nextInt(4)+1) {
		case 1:
			request.setAttribute("checkedAddition", "checked");
			request.setAttribute("checkedSubtraction", "");
			request.setAttribute("checkedMultiplication", "");
			request.setAttribute("checkedDivision", "");
			result = first+second;			
			request.setAttribute("task", first + " + " + second);
			
			request.setAttribute("first", result-r.nextInt(10));
			request.setAttribute("second", result*r.nextInt(10));
			request.setAttribute("third", result);
			request.setAttribute("fourth", result+r.nextInt(10));
			break;
		case 2:
			request.setAttribute("checkedAddition", "");
			request.setAttribute("checkedSubtraction", "checked");
			request.setAttribute("checkedMultiplication", "");
			request.setAttribute("checkedDivision", "");	
			result = first-second;			
			request.setAttribute("task", first + " - " + second);
			
			request.setAttribute("first",result);
			request.setAttribute("second", result*r.nextInt(10));
			request.setAttribute("third",  result-r.nextInt(10));
			request.setAttribute("fourth", result+r.nextInt(10));
			break;
		case 3:
			request.setAttribute("checkedAddition", "");
			request.setAttribute("checkedSubtraction", "");
			request.setAttribute("checkedMultiplication", "checked");
			request.setAttribute("checkedDivision", "");
			result = first*second;			
			request.setAttribute("task", first + " * " + second);
			
			request.setAttribute("first", result-r.nextInt(10));
			request.setAttribute("second", result*r.nextInt(10));
			request.setAttribute("third", result+r.nextInt(10));
			request.setAttribute("fourth",result );
			break;
		case 4:
			request.setAttribute("checkedAddition", "");
			request.setAttribute("checkedSubtraction", "");
			request.setAttribute("checkedMultiplication", "");
			request.setAttribute("checkedDivision", "checked");	
			result = first/second;			
			request.setAttribute("task", first + " / " + second);
			
			request.setAttribute("first",result );
			request.setAttribute("second", result*r.nextInt(10));
			request.setAttribute("third", result+r.nextInt(10));
			request.setAttribute("fourth",result-r.nextInt(10) );
			break;       
		default:
			result=0;
			break;
		}
			request.setAttribute("result", result);
			request.setAttribute("amountOfTrue", amountOfTrue);
			request.setAttribute("amount", amount);		
			request.setAttribute("thePercentageOfCorrect", thePercentageOfCorrect);		
			rd.forward(request, response);
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
