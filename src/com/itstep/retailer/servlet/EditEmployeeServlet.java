package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.beans.Employee;
import com.itstep.retailer.dao.OfficeDAOImpl;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.MyUtils;

@WebServlet("/editEmployee")
public class EditEmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public EditEmployeeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		String employeeNumber = (String) request.getParameter("employeeNumber");

		Employee employee = null;

		String errorString = null;

		try {
			employee = DBUtils.findEmployee(conn, employeeNumber);
		} catch (SQLException e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}

		if (errorString != null && employee == null) {
			response.sendRedirect(request.getServletPath() + "/employeeList");
			return;
		}

		request.setAttribute("errorString", errorString);
		request.setAttribute("employee", employee);

		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/views/editEmployeeView.jsp");
		dispatcher.forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection conn = MyUtils.getStoredConnection(request);

		int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
		String lastName = (String) request.getParameter("lastName");
		String firstName = (String) request.getParameter("firstName");
		String extension = (String) request.getParameter("extension");
		String email = (String) request.getParameter("email");
		int officeCode = Integer.parseInt(request.getParameter("officeCode"));
		int reportsTo = Integer.parseInt(request.getParameter("reportsTo"));
		String jobTitle = (String) request.getParameter("jobTitle");
		Timestamp lastUpdated = new Timestamp(1000l);
        String reason = request.getParameter("reason");
		Employee employee = new Employee();
		employee.setEmployeeNumber(employeeNumber);
		employee.setLastName(lastName);
		employee.setFirstName(firstName);
		employee.setExtension(extension);
		employee.setEmail(email);
		employee.setOffice(new OfficeDAOImpl().getByKey(officeCode));
		employee.setReportsTo(reportsTo);
		employee.setJobTitle(jobTitle);;
		employee.setLAST_UPDATED(lastUpdated);
		String errorString = null;
        String whatToDo = "Edit";
		try {
			DBUtils.updateEmployee(conn, employee);
			DBUtils.saveChangesForEmployee(conn, employeeNumber+"", request.getSession(), reason,whatToDo );
		} catch (SQLException e) {
			e.printStackTrace();
			errorString = e.getMessage();
		}
		request.setAttribute("errorString", errorString);
		request.setAttribute("employee", employee);

		if (errorString != null) {
			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/views/editEmployeeView.jsp");
			dispatcher.forward(request, response);
		}

		else {
			response.sendRedirect(request.getContextPath() + "/employeeList");
		}
	}

}
