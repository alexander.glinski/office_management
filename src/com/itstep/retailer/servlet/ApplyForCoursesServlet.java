package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.utils.MyUtils;


@WebServlet("/applyForCourses")
public class ApplyForCoursesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ApplyForCoursesServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String course = request.getParameter("course");
		request.setAttribute("course", course);
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/applyForCourse.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String course = request.getParameter("course");
		String name = request.getParameter("name");
		String surName = request.getParameter("surName");
		String age = request.getParameter("age");
		String email = request.getParameter("email");
		String sql = "INSERT INTO `ApplyStudents` (`id`, `Course`, `STUDENT_NAME`, `STUDENT_SURNAME`, `AGE`, `email`) VALUES (NULL, ?, ?, ?, ?, ?)";
		Connection conn = MyUtils.getStoredConnection(request);
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, course);
			pstm.setString(2, name);
			pstm.setString(3, surName);
			pstm.setString(4, age);
			pstm.setString(5, email);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher("/WEB-INF/views/welcome.jsp");
		dispatcher.forward(request, response);
	}

}
