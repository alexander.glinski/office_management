package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.beans.ProductLines;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.MyUtils;

/**
 * Servlet implementation class CreateProductLinesServlet
 */
@WebServlet("/createProductLines")
public class CreateProductLinesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateProductLinesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/createProductLinesView.jsp");
        dispatcher.forward(request, response);
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);
 
        String productLine = (String) request.getParameter("productLine");
        String textDescription = (String) request.getParameter("textDescription");
        String htmlDescription = (String) request.getParameter("htmlDescription");
        String image = (String) request.getParameter("image");       
        ProductLines pl = new ProductLines(productLine,textDescription,htmlDescription,image);
 
        String errorString = null;
        String regex = "\\w+";
 
        if (productLine == null || !productLine.matches(regex)) {
            errorString = "ProductLines productLine invalid!";
        }
 
        if (errorString == null) {
            try {
                DBUtils.insertProductLines(conn, pl);
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            }
        }

        request.setAttribute("errorString", errorString);
        request.setAttribute("productLines", pl);

        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/createProductLinesView.jsp");
            dispatcher.forward(request, response);
        }

        else {
            response.sendRedirect(request.getContextPath() + "/productLinesList");
        }
    }

}
