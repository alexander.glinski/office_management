package com.itstep.retailer.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.utils.AppSettings;
import com.itstep.retailer.utils.MailUtils;
import com.itstep.retailer.utils.MyUtils;

@WebServlet("/reportProblem")
public class ReportProblemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ReportProblemServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderNumber = request.getParameter("orderNumber");
		HttpSession session  = request.getSession();
		 UserAccount userInSession = MyUtils.getLoginedUser(session);
		 RequestDispatcher rd= request.getRequestDispatcher("/WEB-INF/views/reportProblemView.jsp");
			
		 if(userInSession!=null) {
			 request.setAttribute("userName", userInSession.getUserName());
			 request.setAttribute("orderNumber", orderNumber);
		 } else {
			 rd= request.getRequestDispatcher("/WEB-INF/views/loginView.jsp");				
		 }		 
		 rd.forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String orderNumber = request.getParameter("orderNumber");
		String userName = request.getParameter("userName");
		String problem = request.getParameter("problem");
		RequestDispatcher rd= request.getRequestDispatcher("/WEB-INF/views/problemWasSended.jsp");
		String message ="Something wrong";
		if(orderNumber!= null && userName!=null) {
			String subject = "Some problem. User: " + userName+ ", with orderNumber: " + orderNumber;
	        String fromEmail = AppSettings.getSettings().getPropValue("mailUser");
	        String pass = AppSettings.getSettings().getPropValue("mailPass");	        
			MailUtils.send(fromEmail, pass, fromEmail, subject, problem);
			message = "Problem was Sended. Please wait";
		}
		request.setAttribute("message", message);
		rd.forward(request, response);
		
	}

}
