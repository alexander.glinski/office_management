package com.itstep.retailer.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.beans.Role;
import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.jdbc.UserAccountMapper;
import com.itstep.retailer.utils.AppSettings;
import com.itstep.retailer.utils.CryptoUtils;
import com.itstep.retailer.utils.MailUtils;


@WebServlet("/reg")
public class RegServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public RegServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/registerView.jsp");
        dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
		Connection conn  = DBUtils.getConnection();
		UserAccountMapper userMapper = new UserAccountMapper(conn);
		String userName = request.getParameter("txt_userName");
        String password = request.getParameter("txt_password");
        String gender = request.getParameter("txt_gender");
        String email  = request.getParameter("txt_email");
        String errorString = null;
        if(userName.length()==0 && password.length()==0 && gender.length()==0 && email.length()==0) {
        	RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/registerView.jsp");
        	errorString = "Something wrong.Please insert real values";
        	request.setAttribute("errorString", errorString);
            dispatcher.forward(request, response);
            
        } else {
            UserAccount user = new UserAccount(userName,gender,password,Role.NO_PERMISSIONS,email,"NO");				
            userMapper.create(user);
            
            try {
				user = DBUtils.findUser(conn, userName, password);
			} catch (Exception e) {
				errorString = e.getMessage();
				e.printStackTrace();
			}
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
            if(errorString!=null) {
            	dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/registerView.jsp");           	
            }
            request.setAttribute("errorString", errorString);
            request.setAttribute("user", user);
            File emailSend = new File("D:\\SimpleJSPProject\\SimpleJSPProject\\properties\\emailSend.txt");
            String message="";
            try{
            	   FileInputStream fstream = new FileInputStream(emailSend);
            	   BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            	   String strLine;
            	   while ((strLine = br.readLine()) != null){
            		   message+=strLine;
            	   }
            	}catch (IOException e){
            	   System.out.println("Ошибка");
            	}
            try {
		        String fromEmail = AppSettings.getSettings().getPropValue("mailUser");
		        String pass = AppSettings.getSettings().getPropValue("mailPass");
				CryptoUtils cu = new CryptoUtils();
				System.out.println(password);
				String strToEnctypt = "2"+password+"4";
				String key = cu.encrypt(strToEnctypt);
				String link = "http://localhost:8080/app/confirmEmail?userName="+user.getUserName()+"&"+"key="+key;
		        message = message.replace("{KEY}", link).replaceAll("     ", " ").replaceAll("     ", " ").replaceAll("     ", " ").replaceAll("     ", " ").replaceAll("     ", " ").replaceAll("     ", " ").replaceAll("  ", " ").trim();
				String subject = "Confirm registration in App";
				MailUtils.sendEmailWithAttachment(fromEmail, pass, email, subject, message,null);
				request.setAttribute("isEmailSend", "was Send");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
				rd.forward(request, response);
			} catch (Exception e) {
				request.setAttribute("isEmailSend", "not Send");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
				rd.forward(request, response);
				e.printStackTrace();
			}
        }

        
	}

}
