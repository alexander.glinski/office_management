package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.CryptoUtils;



@WebServlet("/confirmEmail")
public class ConfirmEmailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ConfirmEmailServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String userName = request.getParameter("userName");
		String keyInGet = request.getParameter("key");		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/userInfoView.jsp");
		Connection conn = DBUtils.getConnection();
		UserAccount user = null;
		if(userName==null && keyInGet==null) {			
			rd.forward(request, response);
		} else {
			rd = request.getRequestDispatcher("/WEB-INF/views/emailIsConfirm.jsp");
			try {
				 user = DBUtils.findUser(conn, userName);
				CryptoUtils cu = new CryptoUtils();
				String strToEnctypt = "2"+cu.decrypt(user.getPassword())+"4";
				String key = cu.encrypt(strToEnctypt);
				key = key.replace("+", "");
				key = key.replaceAll("\\s+","");
				keyInGet = keyInGet.replace("+", "");
				keyInGet = keyInGet.replaceAll("\\s+","");
				if(key.equals(keyInGet)) {
					DBUtils.confirmEmail(conn, userName);
					System.out.println("Email подтвержден");
					request.setAttribute("user", user);
					request.setAttribute("emailIsConfirm", "Email is Confirm");
					 user = DBUtils.findUser(conn, userName);
				} else {
					
					request.setAttribute("emailIsConfirm", "Email is not Confirm");
				}
			} catch (Exception e) {

				
				e.printStackTrace();
			} finally {
				request.setAttribute("user", user); 
				request.setAttribute("loginedUser", user);
				rd.forward(request, response);
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
