package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itstep.retailer.beans.UserAccount;
import com.itstep.retailer.jdbc.DBUtils;

/**
 * Servlet implementation class BuyProductServlet
 */
@WebServlet("/buyProduct")
public class BuyProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public BuyProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				String userName = request.getParameter("userName");
				String code = request.getParameter("code");
				Connection conn = DBUtils.getConnection();
				try {
					DBUtils.buyProduct(conn, userName,code);
					UserAccount user = DBUtils.findUser(conn, userName);
					request.setAttribute("user", user);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                response.sendRedirect("http://localhost:8080/app/userInfo");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
