package com.itstep.retailer.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itstep.retailer.beans.Employee;
import com.itstep.retailer.dao.OfficeDAOImpl;
import com.itstep.retailer.jdbc.DBUtils;
import com.itstep.retailer.utils.MyUtils;

/**
 * Servlet implementation class CreateEmployeeServlet
 */
@WebServlet("/createEmployee")
public class CreateEmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateEmployeeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
        RequestDispatcher dispatcher = request.getServletContext()
                .getRequestDispatcher("/WEB-INF/views/createEmployeeView.jsp");
        dispatcher.forward(request, response);
    }
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	Connection conn = MyUtils.getStoredConnection(request);
    	int officeCode=1;
    	int reportsTo=333;
		int employeeNumber = Integer.parseInt(request.getParameter("employeeNumber"));
		String lastName = (String) request.getParameter("lastName");
		String firstName = (String) request.getParameter("firstName");
		String extension = (String) request.getParameter("extension");
		String email = (String) request.getParameter("email");
		 String reason = request.getParameter("reason");
		String whatToDo = "Create";
		if(request.getParameter("officeCode").trim().length()==0) {
		} else {
			officeCode = Integer.parseInt(request.getParameter("officeCode"));
		}
		if(request.getParameter("reportsTo").trim().length()==0) {
		} else {
			reportsTo = Integer.parseInt(request.getParameter("reportsTo"));
		}
		String jobTitle = (String) request.getParameter("jobTitle");
		Timestamp lastUpdated = new Timestamp(1000l);

		Employee employee = new Employee();
		employee.setEmployeeNumber(employeeNumber);
		employee.setLastName(lastName);
		employee.setFirstName(firstName);
		employee.setExtension(extension);
		employee.setEmail(email);
		employee.setOffice(new OfficeDAOImpl().getByKey(officeCode));
		employee.setReportsTo(reportsTo);
		employee.setJobTitle(jobTitle);;
		employee.setLAST_UPDATED(lastUpdated);
       
        String errorString = null;
        String regex = "\\w+";
        String employeeNumber1 = employeeNumber+"";
        if (employeeNumber1 == null || !employeeNumber1.matches(regex)) {
            errorString = "employee Number invalid!";
        }
 
        if (errorString == null) {
            try {
                DBUtils.insertEmployee(conn, employee);
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            }
            try {
                DBUtils.saveChangesForEmployee(conn, employeeNumber+"", request.getSession(), reason,whatToDo);
            } catch (SQLException e) {
                e.printStackTrace();
                errorString = e.getMessage();
            } 
        }
        

        request.setAttribute("errorString", errorString);
        request.setAttribute("employee", employee);

        if (errorString != null) {
            RequestDispatcher dispatcher = request.getServletContext()
                    .getRequestDispatcher("/WEB-INF/views/createEmployeeView.jsp");
            dispatcher.forward(request, response);
        }

        else {
            response.sendRedirect(request.getContextPath() + "/employeeList");
        }
    }

}
